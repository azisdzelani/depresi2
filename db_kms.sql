-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2016 at 08:46 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_berita`
--

CREATE TABLE IF NOT EXISTS `tbl_berita` (
  `id_berita` int(11) NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_kategori_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_berita`
--

INSERT INTO `tbl_berita` (`id_berita`, `id_user`, `id_kategori_berita`, `judul`, `isi`, `gambar`, `tanggal`) VALUES
(13, 7, 10, 'Jutaan US Dollar Hasil Laut Indonesia Rambah Pasar Spanyol', '<p><strong>Vigo</strong>&nbsp;- Indonesia terus berusaha menggenjot ekspor hasil lautnya ke pasar internasional. Pada pameran di Vigo, Spanyol, diperoleh nilai transaksi US$ 10.046.000, naik dua kali lipat dari tahun sebelumnya senilai US$ 5 juta.<br />\r\n<br />\r\n&quot;Sebelumnya pada periode Januari-Juni 2016 tercatat angka ekspor produk hasil laut Indonesia ke Spanyol sebesar US$ 13,3 juta, naik 52,7% dibandingkan periode sama tahun 2015 senilai US$ 8,7 juta,&quot; ujar Atase Perdagangan KBRI Madrid Elisa Rosma kepada detikcom Den Haag, Sabtu (8/10/2016) seusai pameran.<br />\r\n<br />\r\nLa Feria Internacional de Productos del Mar Congelados (Pameran Internasional Produk Beku Hasil Laut) di Vigo, Spanyol, merupakan pameran terbesar kedua di dunia untuk produk hasil laut dan hasil laut beku.&nbsp;<br />\r\n<br />\r\n&quot;Hasil laut Indonesia yang diminati selain ikan tuna ekor kuning dan ikan tuna skipjack yang sudah terkenal, juga cumi-cumi dan gurita yang permintaannya meningkat tajam,&quot; imbuh Rosma.<br />\r\n<br />\r\nMenurut Rosma, pasar Spanyol mempunyai daya tarik tersendiri karena meskipun Spanyol merupakan negara penghasil produk hasil laut, namun untuk memenuhi permintaan domestik yang tinggi Spanyol harus banyak mengimpor hasil laut dari negara lain.<br />\r\n<br />\r\nSaat ini Indonesia termasuk 4 negara Asia pemasok besar hasil laut ke pasar Spanyol dengan posisi di peringkat 41. Negara Asia lainnya yang peringkatnya lebih tinggi adalah China (4), India (13), dan Vietnam (23).<br />\r\n<br />\r\n&quot;KBRI Madrid berharap melalui pameran ini Indonesia dapat meningkatkan peringkatnya sebagai mitra dagang Spanyol untuk produk seafood,&quot; demikian Rosma.<br />\r\n<br />\r\nPartisipasi Indonesia pada pameran ini diwakili 6 perusahaan eksportir hasil laut dengan fasilitasi dari Atase Perdagangan KBRI Madrid dan Indonesian Trade Promotion Center (ITPC) Barcelona.<br />\r\n<br />\r\nPameran yang diselenggarakan oleh Konfederasi Industri Perikanan Spanyol (Conxemar) tersebut dibuka oleh Menteri Negara Perikanan Spanyol Andres Hermida dan Walikota Abel Caballero.</p>\r\n\r\n<p>Menurut Presiden Conxemar Jose Luis Freire, pameran kali ini diikuti oleh 583 perusahaan dari 156 negara dan dikunjungi oleh 30.020 pembeli internasional dan profesional.&nbsp;<br />\r\n<br />\r\n&quot;Lebih sukses dibandingkan tahun 2015, seperti terlihat pada jumlah peserta di mana pada waktu itu hanya diikuti oleh 520 perusahaan dari 105 negara, dan dikunjungi hanya oleh 27.000 pembeli,&quot; pungkas Freire.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'no_image.jpg', '2016-10-09 02:59:40'),
(14, 7, 10, 'Sabita Dipulangkan dalam Kondisi Selamat Tanpa Luka', '<p><strong>Malang</strong>&nbsp;- Sabita Maufidah Lailia bocah berusia 3,5 tahun dipulangkan oleh orang tak dikenal selang 30 jam pasca diculik dua pria dari rumah orang tuanya.<br />\r\n<br />\r\nBelum diketahui jelas siapa pengendara motor Yamaha Vixion warna putih yang diduga kuat menurunkan korban di tempat sepi tidak jauh dari rumah orang tuanya di Dusun Nongkosewu, Desa Karangnongko, Kecamatan Poncokusumo, Kabupaten Malang, Minggu (23/10/2016), malam.<br />\r\n<br />\r\n&quot;Korban diturunkan di tempat sepi tidak jauh dari rumahnya. Saksi yang melihat ada pengendara motor yang menurunkan di tempat itu,&quot; ujar Kasatreskrim Polres Malang AKP Adam Purbantoro kepada detikcom, Minggu malam.<br />\r\n<br />\r\nKepulangan Sabita tidak menghentikan aparat kepolisian untuk memburu pelaku. Upaya pengejaran pun dilakukan hingga saat ini.&nbsp;<br />\r\n<br />\r\n&quot;Pelaku masih lakukan pengejaran,&quot; tegasnya.<br />\r\n<br />\r\nAdam juga menyebut, Sabita kembali dalam kondisi selamat. Tidak ditemukan bekas luka kekerasan oleh penculik. Selain itu, lanjut dia, Sabita pulang masih mengenakan pakaian yang dikenakan terakhir saat dibawa kabur penculik dari rumahnya, Sabtu (22/10/2016), siang.<br />\r\n<br />\r\n&quot;Korban dalam kondisi selamat. Tidak ada tanda-tanda kekerasan pada tubuh korban,&quot; jelas Adam.<br />\r\n<br />\r\nSabita diduga dikembalikan di saat aparat kepolisian memburu jejak para pelaku. Apa motif dibalik kepulangan Sabita, masih menjadi PR penegak hukum yang menyelidiki kasus ini. Menurut keterangan yang dihimpun detikcom, pengendara motor Yamaha Vixion warna putih yang diduga komplotan pelaku mendadak menurunkan korban sekitar 50 meter dari rumahnya. Warga yang mengetahuinya, dan mendatangi seorang bocah yang diturunkan dari motor langsung mengetahui itu adalah Sabita sempat mengejar pelaku. Tapi sebelum dapat dibekuk warga, pengendara motor berhasil meloloskan diri.<br />\r\n<br />\r\nSeperti diberitakan, Sabita Maufidiah Lailia bocah berusia 3,5 tahun diculik dua orang tak dikenal dari rumah orang tuanya di Dusun Nongkosewu, Desa Karangnongko, Kecamatan Poncokusumo, Kabupaten Malang, Sabtu (22/10/2016), siang. Korban dibawa kabur pelaku saat tengah terlelap tidur di depan ruang keluarga untuk menonton televisi. Salah satu pelaku sempat menganiaya dan menyekap Kuma&#39;iyah, pembantu di rumah pasangan suami istri Satria Pamungkas dan Yuyun Maulidyah.<br />\r\n<br />\r\nPasca penculikan, keluarga sempat menerima pesan singkat dari nomor yang tidak dikenali. Dari keterangan saksi, para pelaku mengendari mobil Nissan March warna abu-abu.</p>\r\n', '14639654_1853327251553481_3584033146994109666_n.jpg', '2016-10-24 00:09:02'),
(15, 7, 10, 'Satu Bom yang Gagal Meledak di Tembok LP Lhokseumawe Diamankan', '<p><strong>Jakarta</strong>&nbsp;- Dua bom rakitan berdaya ledak rendah dipasang di tembok pagar LP Kelas 2 A Lhokseumawe, Aceh. Satu meledak dan satu lagi belum sempat meledak. Kini bahan peledak yang terbuat dari kaleng makanan itu sudah diamankan petugas.<br />\r\n<br />\r\nBerdasarkan foto yang diperoleh detikcom, sebuah kaleng makanan (sarden) tergeletak di lokasi ledakan. Ada sumbu yang masih terlihat pada kaleng sarden tersebut. Bahan peledak tersebut menggunakan sumbu yang harus dibakar. Namun belum sempat meledak.<br />\r\n<br />\r\nPetugas polisi dan Brimob turun ke lokasi tak lama setelah ledakan terjadi. Berdasarkan hasil olah Tempat Kejadian Perkara (TKP) yang dilakukan polisi bersama tim penjinak bom Brimob Jeulikat, petugas menemukan dua jenis bom rakitan bersifat low eksplosif di lokasi.&nbsp;<br />\r\n<br />\r\nSalah satu bom rakitan menggunakan pemicu dari baterai dan sudah meledak. Sementara satu lagi memakai sistem pemicu menggunakan sumbu atau dibakar dan belum meledak. Bom tersebut dipasang pada tembok akhir LP. Di sana memang terdapat lubang untuk mensuplai pipa air PDAM ke dalam LP.&nbsp;<br />\r\n<br />\r\nDua orang diduga pelaku meletakkan benda diduga bom pada lubang tersebut dan kemudian diledakkan untuk memperbesar lubang. Tujuannya, diduga untuk melarikan diri. Ledakan terjadi sekitar pukul 14.30 WIB, Minggu (23/10/2016) siang tadi.<br />\r\n<br />\r\nKedua orang diduga pelaku bernama Fauzi dan Tarmizi mengalami luka-luka. Telapak tangan Fauzi putus sementara Tarmizi kena serpihan. Badan keduanya berlumuran darah. Tak lama setelah kejadian, kedua napi kasus sabu tersebut diboyong ke rumah sakit dengan pengawalan polisi.<br />\r\n<br />\r\n&quot;Di lokasi sudah dipasang garis polisi, dan barang bukti sudah diamankan,&quot; kata Kabid Humas Polda Aceh, Kombes Goenawan kepada detikcom.<br />\r\n&nbsp;</p>\r\n', '27_-_Pengaturan_Akun_mp4_snapshot_04_47_2016_10_07_00_06_33.jpg', '2016-10-24 00:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE IF NOT EXISTS `tbl_blog` (
  `id_blog` int(11) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `judul_blog` varchar(50) NOT NULL,
  `isi_blog` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`id_blog`, `id_pegawai`, `judul_blog`, `isi_blog`, `date`) VALUES
(3, 2, 'Bagaimana Cara Menyalakan Komputer', 'Bagaimana cara menyalakan komputer diruangan saya yah, mati nih', '2016-10-08 18:25:21'),
(4, 3, 'Bagaimana Cara Membuat Laporan?', 'Apakah ada yang sudah berpengalaman membuat laporan pengeluaran harian', '2016-10-08 18:25:21'),
(5, 7, 'Dimana Tes?', 'loreminpuem loreminpuemloreminpuemloreminpuemloreminpuemloreminpuem', '2016-10-09 07:53:22'),
(6, 7, 'tes lagi ahj', 'tes lagi dong ii diskusi apa yah', '2016-10-09 07:58:26'),
(7, 7, 'tes lagi ah 2', 'ayoo di tes lagi dong dong', '2016-10-09 07:58:42'),
(8, 7, 'tes lagi ah 3', 'tes lagi sampe baanyak', '2016-10-09 07:58:55'),
(9, 10, 'tes', 'teasasasasaslasnasas', '2016-10-09 08:02:46'),
(10, 10, 'teralhir', 'terakhir nihh terakhir', '2016-10-09 08:07:55'),
(11, 6, 'tes lagi ahj', 'asasaasasas\r\n', '2016-10-09 10:54:16'),
(12, 7, 'Bagaimana Membuat Database?', 'Saya bingung membuat database di mySql', '2016-10-13 08:47:08'),
(13, 2, 'Bagaimana menyambungkan wifi lantai 7', 'saya ingin menyambungkan wifi lt 7 tetapi tidak konek, apakah masalahnya?', '2016-10-17 08:25:24'),
(14, 7, 'Apakah sudah ada yang pernah membuat jaringan kabe', 'saya sedang mengerjaiakan projek membuat kabel usb yang panjang nya beberapa meter, namun sayan tisak bisa menyambungkan nya, aakah ada', '2016-10-21 19:14:58'),
(15, 7, 'printer di lt 7 koslet', 'saya sedang mengeprint data dokumen namun printer dilt 7 mati, tolong untuk bagian it bisa diperbaiki secapatnya', '2016-10-28 08:10:08'),
(16, 7, 'printer mati', 'printer di ruangan publikasi ga jalan nih ada yang bisa bantuin', '2016-11-07 17:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_diskusi`
--

CREATE TABLE IF NOT EXISTS `tbl_diskusi` (
  `id_diskusi` int(3) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `id_kategori` int(3) NOT NULL,
  `judul_diskusi` varchar(100) NOT NULL,
  `isi_diskusi` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_diskusi`
--

INSERT INTO `tbl_diskusi` (`id_diskusi`, `id_pegawai`, `id_kategori`, `judul_diskusi`, `isi_diskusi`, `tanggal`) VALUES
(1, 10, 1, 'ini blog', '<p>ini blog coyy&nbsp;ini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyyini blog coyy<strong>blo</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-10-09 15:21:43'),
(2, 3, 4, 'ini blog saya', '<p>saya akan membuat blog dengan tema photosop</p>\r\n', '2016-10-10 09:38:12'),
(3, 10, 1, 'ini  blog kedua saya', '<p>ini adalah blog kedua saya&nbsp;</p>\r\n', '2016-10-10 09:40:55'),
(4, 10, 1, 'blog ketiga', '<p>ini adalah blog ketiga saya</p>\r\n', '2016-10-10 09:41:08'),
(5, 10, 4, 'ini blog saya', '<p>ini adalah blog saya di kategori inovasi</p>\r\n', '2016-10-10 09:41:32'),
(6, 3, 2, 'ini diskusi saya', 'ini adalah diskusi saya', '0000-00-00 00:00:00'),
(7, 3, 2, 'ini diskusi saya', 'ini adalah diskusi saya', '0000-00-00 00:00:00'),
(8, 10, 4, 'ini diskusi saya', 'ini diskusi saya', '2016-10-19 00:00:00'),
(9, 6, 4, 'iasais', 'asiasjas', '2016-10-27 00:00:00'),
(10, 6, 2, 'asasasa', 'asasasasas', '2016-10-12 00:00:00'),
(11, 7, 3, 'ini blog inovasi', '<p>saya akan membuat blog inovasi sekarang</p>\r\n', '2016-10-11 10:16:30'),
(12, 7, 4, 'asasa', '', '2016-10-11 14:02:14'),
(13, 2, 1, 'Membuat virtual mechine dengan windows', '<p>isi tutroail nya atau isi artikel nya</p>\r\n', '2016-10-17 15:26:33'),
(14, 3, 1, 'Inilah beberapa Kebijakan-Kebijakan Fundamental MenPanRB Asman Abnur Terkait Manajemen ASN', '<p>Kehadiran MenpanRB Asman Abnur digedung PKP2A II LAN pada Hari Senin 17 Oktober 2016, memberi sebuah kesan tersendiri bagi penulis khususnya terkait dengan beberapa kebijakan yg akan diaktualisasikan selama menjabat sebagai MenPanRB.</p>\r\n\r\n<p>Beberapa hal yg penulis simpulkan terkait dengan kebijakan dan pesan yg ingin beliau sampaikan dihadapan para peserta Diklat kepemimpinan tingkat II maupun Pegawai PKP2A II LAN Dan STIA LAN adalah sebagai berikut;</p>\r\n\r\n<p><strong>Pertama,</strong>Pelayanan Publik kepada masyarakat harus mendapatkan perubahan dan pembenahan, banyak layanan publik selama ini seperti layanan administrasi baik layanan Sim,STNK, Ktp, Ijin usaha dan lain sebagainya, diduga mengandung pungli (pungutan Liar) yang dimanfaatkan oknum-oknum tertentu, Dengan memanfaatkan kewenangan dan jabatan yg dia miliki untuk menarik keuntungan (pungli) kepada masyarakat atas balas jasa layanan yg diberikan kepada yg bersangkutan.</p>\r\n\r\n<p>Melihat maraknya pungli yg kadang terjadi dalam pelayanan publik tersebut maka hal tersebut harus diberantas ,untuk memulainya maka pemberantasan pungli harus bersifat up button artinya pemberantasan pungli harus dimulai dulu dari atas kemudian kebawah.</p>\r\n\r\n<p>memberantas pungli,Memulai dari atas artinya memulai dari pimpinan atau pucuk pimpinan disebuah intitusi harus menjadi pioneer atau orang yg pertama yang mengarahkan seluruh bawahannya untuk tidak menerima pungli dalam bentuk apapun ,untuk mencapai intitusi bebas pungli maka pimpinan harus menjadi contoh jangan sebaliknya justru tidak memberikan contoh yg baik.</p>\r\n\r\n<p>Dan kalau ada bawahan atau sataf yg melakukan pungli maka pimpinan harus bertindak tegas dengan memberikan sanksi yg berat kepada yg bersangkutan sehinggah menjadi contoh bagi yg lain untuk tidak menerima pungli, inilah yg dimaksud stretegi pemberantasan pungli pada layanan public dengan pendekatan up botton ,diimulai dari pimpinan dan pimpinan memegang kata kunci dan political will untuk mengatasi layanan tersebut.</p>\r\n\r\n<p><strong>kedua ,</strong>kebijakan yg cukup bagus nantinya adalah MenpanRB akan menjadikan LAN sebagai holding pendidikan sekolah kedinasan bagi seluruh sekolah kedinasan yg ada dikementerian seperti kementerian dalam Negeri ada IPDN, Dikementerian Hukum dan HAM ada akademi ilmu pemasyarakat dan akademi imigrasi, kementerian keuangan ada STAN dan masih banyak lagi sekolah kedinasan yg penulis tidak sebut satu persatu namun hampir semua kementerian memiliki sekolah kedinasan .</p>\r\n\r\n<p>melihat fenomena tersebut maka MenpanRB ingin menjadikan semua sekolah kedinasan dibawah pengembangan dan pembinaan LAN RI sesuai dengan amanat yg terdapat dalam UU nomor 5 tahun 2014 tentan ASN.</p>\r\n\r\n<p>Kalau melihat fungsi LAN sebagaimana yg terdapat dalam uu ASN maka fungsinya antara lain yaitu pertama pengembang standar kualitas pendidikan pelatihan ASN,kemudian kedua Pembina pendidikan pelatihan kompetensi manajerial ASN.kemudian selain itu wewenanga lain antara lain mencabut izin penyelenggaraan pendidikan , memberikan rekomendasi dan mencabut akreditasi lembaga pendidikan dan pelatihan .</p>\r\n\r\n<p>Melihat fungsi dan kewenangan LAN &nbsp;sebagaiman yg amanatkan oleh Undang Undang ASN maka tentunya keputusan MenpanRB untuk membuat LAN RI menjadi holding pendidikan sangat tepat dan sesuai dengan Kontitusi demi tercapainya lembaga pendidikan yg berintegritas.(bersambung&hellip;..)</p>\r\n', '2016-10-24 18:46:56'),
(15, 6, 1, 'artikel percobaan ckeditor', '<p><img alt="laugh" src="http://localhost:8080/depresi2/assets/ckeditor/plugins/smiley/images/teeth_smile.png" style="height:23px; width:23px" title="laugh" />hai apa kabar semua nya&nbsp;<img alt="heart" src="http://localhost:8080/depresi2/assets/ckeditor/plugins/smiley/images/heart.png" style="height:23px; width:23px" title="heart" />&nbsp;<textarea cols="5" name="tekan disini" required="required" rows="5">masukan alamat</textarea></p>\r\n', '2016-10-25 13:41:48'),
(16, 7, 2, 'Tutorial menulis yang baik untuk bahan publikasi', '<p><img alt="" src="/depresi2/assets/images_artikel/27_Pengaturan_Akun_mp4_snapsho.33].jpg" style="border-style:solid; border-width:1px; float:left; height:100px; margin:1px; width:178px" />Apa kabar semua nya, kali ini saya akan menampilkan gambar disebelah kiri saya, apakah sudah berhasil</p>\r\n', '2016-10-28 20:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dokumen`
--

CREATE TABLE IF NOT EXISTS `tbl_dokumen` (
  `id_dokumen` int(3) NOT NULL,
  `id_kategori_dokumen` int(3) NOT NULL,
  `id_user` int(3) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` datetime NOT NULL,
  `file` text
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dokumen`
--

INSERT INTO `tbl_dokumen` (`id_dokumen`, `id_kategori_dokumen`, `id_user`, `judul`, `keterangan`, `tanggal`, `file`) VALUES
(2, 5, 7, 'Pengolahan Media Center dan Permohonan Informasi', 'Pedoman dalam Pelaksanaan Pengelolaan Media Center dan Pelayanan Informasi ', '2016-02-25 00:00:00', 'SOP-LAN-04_04-PENGELOLAAN-MEDIA-CENTER-DAN-PERMOHO'),
(3, 5, 7, 'Pelayanan HUMAS dan Informasi', 'Pedoman Pelaksanaan Tugas Pengeloaan Data dan Informasi HUMAS', '2016-02-18 00:00:00', 'SOP-PELAYANAN-INFORMASI-DAN-HUBUNGAN-ANTAR-LEMBAGA'),
(4, 5, 7, 'sop', 'ini adalah SOP', '1993-12-04 00:00:00', 'Form-Konversi.pdf'),
(5, 3, 7, 'lakip', 'ini lakip', '1993-12-10 00:00:00', 'CFM_LAN_09_PENGELOLAAN_TEKNOLOGI_INFORMASI_(1).pdf'),
(6, 2, 7, 'notulen', 'ini notulensi', '2016-09-17 00:00:00', '04062014_Notulensi_Rapat_Pengembangan_Web_LAN.docx'),
(7, 2, 7, 'notulen tes', 'ini notulensi tes', '2016-09-02 00:00:00', '23062014_Notulensi_Rapat_E_office.docx'),
(8, 0, 7, '', '                                                      \r\n                          \r\n                        ', '2016-09-24 12:34:19', '344A725CA45060.pdf'),
(9, 12, 7, 'Anjab Pengolah Data & Bahan Publikasi ', 'Menerima, Mencatat, Meneliti, Menyiapkan, dan Menyampaikan Informasi, serta melakukan kegiatan administrasi ringan lainnya sesuai dengan pedoman dan petunjuk atasan untuk kelancaran tugas dan tertib administrasi di Bagian Humas dan Informasi.                            \r\n                        ', '2016-09-25 01:23:25', 'Anjab_Choky.doc'),
(10, 13, 3, 'tes', 'tes                            \r\n                        ', '2016-09-27 16:16:38', 'Anjab_Kasubag_Humas_Budi9.doc'),
(11, 13, 3, 'tes coba', '\r\ntes coba                            \r\n                        ', '2016-09-27 16:49:25', 'Anjab_Kabag_Humas_dan_Informasi_20141.doc'),
(12, 13, 8, 'modul photohop', 'ini adalah modul photoshop yang saya dapat dari pelatihan di kominfo                       ', '2016-10-09 03:29:05', 'Miitomo.docx'),
(13, 13, 7, 'coba dokumen limit', 'ini dokumen limit                            \r\n                        ', '2016-10-13 01:04:40', 'Egg.docx'),
(14, 5, 3, 'tes judul terbaru', 'ini adalah judul terabru                                                      \r\n                          \r\n                        ', '2016-10-22 00:06:44', 'No.docx');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jabatan`
--

CREATE TABLE IF NOT EXISTS `tbl_jabatan` (
  `id_jabatan` int(2) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jabatan`
--

INSERT INTO `tbl_jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
(1, 'Kepala Bagian Humas'),
(2, 'Kepala Sub Bagian Publikasi'),
(3, 'Kepala Sub Bagian TI'),
(4, 'Staff IT'),
(5, 'Staff Publikasi');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori` (
  `id_kategori` int(3) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`, `keterangan`) VALUES
(1, 'teknologi', 'berisi tentang segala macam diskusi yang berkaitan dengan teknologi'),
(2, 'jurnalistik', 'berisi tentang segala macam diskusi yang berkaitan dengan jurnalistik'),
(3, 'Inovasi', 'Membahasa Semua yang Berhubungan Dengan Inovasi di Bidang Administrasi'),
(4, 'Photoshop', 'Membahas Segala Hal tentang Photohop');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori_berita`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori_berita` (
  `id_kategori_berita` int(3) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori_berita`
--

INSERT INTO `tbl_kategori_berita` (`id_kategori_berita`, `nama_kategori`, `keterangan`) VALUES
(7, 'Inovasi', 'Berisi Semua Berita yang Berkaitan dengan Kegiatan Seputar LAN'),
(8, 'Lintas Instansi', 'Berisi Semua Berita yang Berhubungan Dengan Instansi yang berada dibawah LAN'),
(9, 'Informasi LAN', 'Berisi tentang semua berita yang ada di LAN'),
(10, 'Umum', 'Berisi tentang smeua berita umum');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori_dokumen`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori_dokumen` (
  `id_kategori_dokumen` int(3) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori_dokumen`
--

INSERT INTO `tbl_kategori_dokumen` (`id_kategori_dokumen`, `nama_kategori`) VALUES
(2, 'Notulensi'),
(3, 'Lakip'),
(5, 'SOP'),
(9, 'Jadwal Pelatihan'),
(12, 'Hasil Inovasi'),
(13, 'Shared Document'),
(14, 'Tes'),
(15, 'Tes Oi'),
(16, '3'),
(17, '6'),
(18, '6'),
(19, '7'),
(20, 'Yok');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komentar`
--

CREATE TABLE IF NOT EXISTS `tbl_komentar` (
  `id_komentar` int(3) NOT NULL,
  `id_diskusi` int(3) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `isi_komentar` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_komentar`
--

INSERT INTO `tbl_komentar` (`id_komentar`, `id_diskusi`, `id_pegawai`, `isi_komentar`, `date`) VALUES
(1, 1, 6, 'pertamax gan', '2016-10-09 09:59:34'),
(2, 1, 7, 'boong', '2016-10-10 08:17:02'),
(3, 11, 7, 'pertamax gan', '2016-10-11 03:16:41'),
(4, 1, 7, 'udah bisa belom dul?', '2016-10-11 03:22:22'),
(5, 12, 6, 'wah bagus nih tulisan nya', '2016-10-11 14:08:26'),
(6, 12, 2, 'tes', '2016-10-12 03:46:47'),
(7, 4, 7, 'tes komentar', '2016-10-12 21:39:45'),
(8, 6, 2, 'asasas', '2016-10-22 17:36:38'),
(9, 13, 6, 'bagus sekali tutorial nya pak', '2016-10-24 11:02:06'),
(10, 2, 3, 'tes', '2016-10-24 11:14:32'),
(11, 6, 3, 'hel me please', '2016-10-24 11:16:05'),
(12, 13, 3, 'tes', '2016-10-24 11:19:03'),
(13, 7, 3, 'tes', '2016-10-24 11:23:21'),
(14, 14, 3, 'tes komentar', '2016-10-24 11:47:16'),
(15, 2, 6, 'keduax', '2016-10-24 16:40:02'),
(16, 15, 6, 'work gan', '2016-10-25 06:42:20'),
(17, 14, 6, 'mantap gan', '2016-10-25 11:57:55'),
(18, 15, 6, 'keren gan', '2016-10-25 12:01:12'),
(19, 16, 7, 'komentar pertamax', '2016-10-28 13:52:22'),
(20, 15, 10, 'jalan', '2016-11-09 03:13:33'),
(21, 13, 2, 'keren gan tulisan nya', '2016-11-09 15:06:44'),
(22, 16, 2, 'pertamax gan', '2016-11-09 15:56:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komentar_blog`
--

CREATE TABLE IF NOT EXISTS `tbl_komentar_blog` (
  `id_komentar_blog` int(11) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `id_blog` int(11) NOT NULL,
  `isi_komentar` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_komentar_blog`
--

INSERT INTO `tbl_komentar_blog` (`id_komentar_blog`, `id_pegawai`, `id_blog`, `isi_komentar`, `date`) VALUES
(6, 6, 4, 'kalo itu coba tanyakan kebagian keuangan', '2016-10-08 18:25:59'),
(7, 6, 3, 'coba pencet tombol shutdown gan', '2016-10-08 18:30:17'),
(8, 7, 3, 'sundul gan', '2016-10-08 20:23:57'),
(9, 7, 4, 'data laporan untuk refrensi dapat dilihat pada lakip 2015', '2016-10-09 07:09:10'),
(10, 7, 5, 'pertamax gan', '2016-10-09 07:54:28'),
(11, 7, 11, 'hgh', '2016-10-10 07:32:18'),
(12, 7, 4, 'belum, coba tanya atasan nya', '2016-10-11 03:15:51'),
(13, 6, 10, 'pertamax gan', '2016-10-11 14:08:50'),
(14, 7, 11, 'hay', '2016-10-12 21:47:02'),
(15, 7, 12, 'pertamax', '2016-10-13 18:22:29'),
(16, 7, 11, 'pertamax gan', '2016-10-14 19:11:34'),
(17, 2, 12, 'tes komentar', '2016-10-16 12:11:16'),
(18, 2, 3, 'udah bisa belom dul?', '2016-10-16 12:18:11'),
(19, 2, 12, 'tes komentar', '2016-10-17 08:24:32'),
(20, 6, 13, 'komentar', '2016-10-18 05:10:17'),
(21, 7, 13, 'password nya adalah lan.go,ig langsung konek saja', '2016-10-21 19:13:23'),
(22, 7, 14, 'pertamax gan', '2016-10-22 17:05:02'),
(23, 2, 14, 'komentar dong', '2016-10-22 17:33:57'),
(24, 2, 14, 'tes komentar', '2016-10-22 17:34:26'),
(25, 2, 14, 'cek', '2016-10-22 17:34:36'),
(26, 2, 14, 'tes', '2016-10-22 17:34:40'),
(27, 2, 14, 'asasa', '2016-10-22 17:35:37'),
(28, 6, 14, 'belom pernah gan', '2016-10-28 04:42:51'),
(29, 7, 14, 'tes komentar', '2016-10-28 07:30:38'),
(30, 7, 15, 'bagi nanti saya akan koordinasikan staff saya untuk kesana', '2016-10-28 13:34:29'),
(31, 7, 13, 'komentar', '2016-10-28 13:45:48'),
(32, 2, 15, 'oke saya kesana yah', '2016-10-30 16:06:33'),
(33, 7, 14, 'tolong min kasih tag sudah terjawab', '2016-10-30 20:30:57'),
(34, 11, 15, 'pertamax gan', '2016-11-10 10:20:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pegawai`
--

CREATE TABLE IF NOT EXISTS `tbl_pegawai` (
  `id_pegawai` int(3) NOT NULL,
  `nip` varchar(18) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan','','') NOT NULL,
  `agama` varchar(10) NOT NULL,
  `id_jabatan` int(2) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat_rumah` varchar(50) NOT NULL,
  `telepon` varchar(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pegawai`
--

INSERT INTO `tbl_pegawai` (`id_pegawai`, `nip`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `agama`, `id_jabatan`, `email`, `alamat_rumah`, `telepon`) VALUES
(2, '196609291986022002', 'Budi Prayitno, SIP, M.SI', 'Purwokerto', '1989-12-04', 'laki-laki', 'islam', 2, 'prayitno@gmail.com', 'jalan raya perintis', '085693677158'),
(3, '196609291986022003', 'Danang Dwi Cahyono, S.os', 'Banyuwangi', '1980-04-05', 'laki-laki', 'islam', 5, 'danang@gmail.com', 'jalan raya menteng ', '085693677158'),
(6, 'admin', 'azis dzelani', 'bekasi', '1993-12-04', 'laki-laki', 'islam', 4, 'azisdzelani@gmail.com', 'jalan raya hankam', '085693677158'),
(7, '1111093000034', 'Ari Noviyanto', 'Purworejo', '1976-12-04', 'laki-laki', 'islam', 3, 'ari@gmail.com', 'jalan raya pendidikan', '085693677158'),
(10, '1111093000035', 'bedul', 'bekasi', '1993-12-04', 'laki-laki', 'islam', 1, 'bedul@gmail.com', 'jalan raya                                        ', '085693677158'),
(11, '1111093000040', 'dhani', 'banyuwangi', '1993-12-04', 'laki-laki', 'kristen', 3, 'dhani@gmail.com', '                                                  ', '085693677159');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pelatihan`
--

CREATE TABLE IF NOT EXISTS `tbl_pelatihan` (
  `id_pelatihan` int(11) NOT NULL,
  `nama_pelatihan` varchar(50) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `tempat` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` enum('pelatihan','workshop','seminar','pendidikan') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pelatihan`
--

INSERT INTO `tbl_pelatihan` (`id_pelatihan`, `nama_pelatihan`, `tanggal_mulai`, `tanggal_selesai`, `tempat`, `deskripsi`, `status`) VALUES
(1, 'pelatihan word', '2016-10-05', '2016-10-10', 'uin', 'ini adalah pelatihan word yang dilaksanakan di uin jakarta, peserta nya 50 orang ', 'pelatihan'),
(2, 'pelatihan exel', '2016-10-13', '2016-10-14', 'uin bandung', 'ini adalah pelatihan exel yang diselenggrakan oleh uin bandung, peserta nya ada 100 orang', 'workshop'),
(3, 'worksehop desain', '2016-11-09', '2016-11-16', 'uin', 'sdsdsdsdsd', 'workshop'),
(4, 'pelatihan menulis', '2016-11-11', '2016-11-18', 'uin jakart', 'kuota 5 orang, 3 orang staff it, 2 orang staff humas', 'pelatihan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_peserta_pelatihan`
--

CREATE TABLE IF NOT EXISTS `tbl_peserta_pelatihan` (
  `id_peserta_pelatihan` int(11) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `id_pelatihan` int(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_peserta_pelatihan`
--

INSERT INTO `tbl_peserta_pelatihan` (`id_peserta_pelatihan`, `id_pegawai`, `id_pelatihan`, `keterangan`) VALUES
(1, 10, 1, 'ini pelatihan word'),
(2, 2, 1, 'ini pelatihan word'),
(3, 7, 1, 'ini pelatihan word'),
(4, 6, 1, 'ini pelatihan word'),
(5, 10, 2, 'ini adalah peserta yang akan mengikuti pelatihan exel yang akan dilakukan di gedung kominfo lt.7'),
(6, 2, 2, 'ini adalah peserta yang akan mengikuti pelatihan exel yang akan dilakukan di gedung kominfo lt.7'),
(7, 7, 2, 'ini adalah peserta yang akan mengikuti pelatihan exel yang akan dilakukan di gedung kominfo lt.7'),
(8, 7, 0, 'sdasd'),
(9, 10, 1, 'bagi yang tidak bisa mengikuti pelatihan tersebut diatas dapat memberitahukan kepada kasubag'),
(10, 2, 1, 'bagi yang tidak bisa mengikuti pelatihan tersebut diatas dapat memberitahukan kepada kasubag'),
(11, 7, 1, 'bagi yang tidak bisa mengikuti pelatihan tersebut diatas dapat memberitahukan kepada kasubag'),
(12, 3, 3, 'yang sudah ikut peletihan desain akan diikut sertakan dalam pimnas'),
(14, 10, 1, 'tes'),
(15, 2, 1, 'tes'),
(16, 7, 1, 'tes'),
(17, 7, 4, 'tes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` int(3) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `level_user` enum('Admin','Kepala Bagian Humas','Kepala Sub Bagian','Staff IT','Staff Publikasi') NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` set('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `id_pegawai`, `level_user`, `username`, `password`, `status`) VALUES
(3, 2, 'Staff IT', '196609291986022002', 'staffit', 'Aktif'),
(4, 3, 'Staff Publikasi', '196609291986022003', 'staffpublikasi', 'Aktif'),
(7, 6, 'Admin', 'admin', 'admin', 'Aktif'),
(8, 7, 'Kepala Sub Bagian', '1111093000034', 'kasubag', 'Aktif'),
(11, 10, 'Kepala Bagian Humas', '1111093000035', 'kabag', 'Aktif'),
(12, 11, 'Kepala Sub Bagian', '1111093000040', 'bedul', 'Aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_berita`
--
ALTER TABLE `tbl_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `tbl_diskusi`
--
ALTER TABLE `tbl_diskusi`
  ADD PRIMARY KEY (`id_diskusi`);

--
-- Indexes for table `tbl_dokumen`
--
ALTER TABLE `tbl_dokumen`
  ADD PRIMARY KEY (`id_dokumen`);

--
-- Indexes for table `tbl_jabatan`
--
ALTER TABLE `tbl_jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_kategori_berita`
--
ALTER TABLE `tbl_kategori_berita`
  ADD PRIMARY KEY (`id_kategori_berita`);

--
-- Indexes for table `tbl_kategori_dokumen`
--
ALTER TABLE `tbl_kategori_dokumen`
  ADD PRIMARY KEY (`id_kategori_dokumen`);

--
-- Indexes for table `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `tbl_komentar_blog`
--
ALTER TABLE `tbl_komentar_blog`
  ADD PRIMARY KEY (`id_komentar_blog`);

--
-- Indexes for table `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tbl_pelatihan`
--
ALTER TABLE `tbl_pelatihan`
  ADD PRIMARY KEY (`id_pelatihan`);

--
-- Indexes for table `tbl_peserta_pelatihan`
--
ALTER TABLE `tbl_peserta_pelatihan`
  ADD PRIMARY KEY (`id_peserta_pelatihan`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_berita`
--
ALTER TABLE `tbl_berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `id_blog` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_diskusi`
--
ALTER TABLE `tbl_diskusi`
  MODIFY `id_diskusi` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_dokumen`
--
ALTER TABLE `tbl_dokumen`
  MODIFY `id_dokumen` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_jabatan`
--
ALTER TABLE `tbl_jabatan`
  MODIFY `id_jabatan` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_kategori_berita`
--
ALTER TABLE `tbl_kategori_berita`
  MODIFY `id_kategori_berita` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_kategori_dokumen`
--
ALTER TABLE `tbl_kategori_dokumen`
  MODIFY `id_kategori_dokumen` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
  MODIFY `id_komentar` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tbl_komentar_blog`
--
ALTER TABLE `tbl_komentar_blog`
  MODIFY `id_komentar_blog` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  MODIFY `id_pegawai` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_pelatihan`
--
ALTER TABLE `tbl_pelatihan`
  MODIFY `id_pelatihan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_peserta_pelatihan`
--
ALTER TABLE `tbl_peserta_pelatihan`
  MODIFY `id_peserta_pelatihan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
