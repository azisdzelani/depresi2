<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIMPEN :: <?=$title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=base_url() ?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url() ?>assets/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?=base_url() ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url() ?>assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/bootstrap/css/custome.css">

  <!-- favivon-->
  <link rel="icon" href="<?=base_url()?>/logo-lan.png" type="image/png">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>K</b>MS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Knowledge</b>Manajemen</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i> <?=$this->session->userdata('nama') ?>, <strong>[ <?=$this->session->userdata('level_user')?> ] <i class="fa fa-caret-down"></i></strong> 
            </a>
            <ul class="dropdown-menu">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <?php 
                    if ($this->session->userdata('level_user') == 'Admin') {
                  ?>
                  <li><a href="<?=base_url('user/ubah_password') ?>"> Ubah Password</a></li>
                  <li><a href="<?=base_url('user') ?>"> Manage User</a></li>
                  <li><a href="<?=base_url('dokumen') ?>"> Manage Document</a></li>
                  <li><a href="<?=base_url('berita/admin_view') ?>"> Manage Berita</a></li>
                  <li><a href="<?=base_url('kategori') ?>"> Manage Category</a></li>
                  <?php } ?>
                  <!-- menu admin end -->

                  <?php 
                    if ($this->session->userdata('level_user') == 'Kepala Bagian Humas') {
                  ?>
                  <li><a href="<?=base_url('user/ubah_password') ?>"> Ubah Password</a></li>
                  <li><a href="<?=base_url('informasi/info_pelatihan') ?>"> Manage Info Pelatihan</a></li>
                  <?php } ?>
                  <!-- menu kabag end -->

                  <?php 
                    if ($this->session->userdata('level_user') == 'Kepala Sub Bagian Publikasi') {
                  ?>
                  <li><a href="<?=base_url('user/ubah_password') ?>"> Ubah Password</a></li>
                  <li><a href="<?=base_url('pelatihan') ?>"> Manage Peserta Pelatihan</a></li>
                  <?php } ?>
                  <!-- menu kasubag end -->

                  <?php 
                    if ($this->session->userdata('level_user') == 'Staff IT') {
                  ?>
                  <li><a href="<?=base_url('user/ubah_password') ?>"> Ubah Password</a></li>
                  <?php } ?>
                  <!-- menu staff end -->

                  <?php 
                    if ($this->session->userdata('level_user') == 'Staff Publikasi') {
                  ?>
                  <li><a href="<?=base_url('user/ubah_password') ?>"> Ubah Password</a></li>
                  <?php } ?>
                  <!-- menu staff end -->
                </ul>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">

        <li class="header">MAIN NAVIGATION</li>
        <li style="padding-left:50px">
          <a href="<?=base_url('home') ?>" data-toggle="tooltip" data-placement="right" title="home"><i class="fa fa-home fa-3x"></i> </a>
        </li>
        <li style="padding-left:50px">
          <a href="<?=base_url('dokumen/dokumen_user') ?>" data-toggle="tooltip" data-placement="right" title="dokumen"><i class="fa fa-book fa-3x"></i> </a>
        </li>
        <li style="padding-left:50px">
          <a href="<?=base_url('blog') ?>" data-toggle="tooltip" data-placement="right" title="discussion"><i class="fa fa-comments fa-3x"></i> </a>
        </li>
        <li style="padding-left:50px">
          <a href="<?=base_url('discussion') ?>" data-toggle="tooltip" data-placement="right" title="blog"><i class="fa fa-pencil fa-3x"></i> </a>
        </li>
        <li style="padding-left:50px">
          <a href="<?=base_url('berita') ?>" data-toggle="tooltip" data-placement="right" title="berita"><i class="fa fa-newspaper-o fa-3x"></i> </a>
        </li>

        <!-- menu kasubag -->
        <?php 
          if ($this->session->userdata('level_user') == 'Kepala Sub Bagian Publikasi') {
        ?>
        <li style="padding-left:50px">
          <a href="<?=base_url('report') ?>" data-toggle="tooltip" data-placement="right" title="laporan"><i class="fa fa-line-chart fa-3x"></i></a>
        </li>
        <?php } ?>
        <!-- menu kasubag end -->

        <li style="padding-left:50px">
          <a href="<?=base_url('informasi') ?>" data-toggle="tooltip" data-placement="right" title="informasi"><i class="fa fa-info-circle fa-3x"></i> </a>
        </li> 
        <li style="padding-left:50px">
          <a href="<?=base_url('auth/logout') ?>" data-toggle="tooltip" data-placement="right" title="logout"><i class="fa fa-power-off fa-3x"></i></a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <?= $this->template->content; ?>
    <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2016 <a href="http://lan.go.id">Lembaga Administrasi Negara</a>.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?=base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url() ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url() ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url() ?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url() ?>assets/dist/js/demo.js"></script>
<!-- page script -->
<script src="<?=base_url() ?>assets/ckeditor/ckeditor.js"></script>

<!-- script page -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
    var roxyFileman = '<?=base_url() ?>assets/ckeditor/plugins/fileman/index.php';
    $(function () {
        CKEDITOR.replace('ckeditor1', {filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'});
    });
</script>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Kategori', 'Total Kategori'],
      <?php foreach($diskusi as $d):?>
      ['<?=$d->nama_kategori?>', <?php $this->load->model('model_report','report'); $total_kategori = $this->report->total_kategori($d->id_kategori);?>
        <?=$total_kategori?>],
      <?php endforeach; ?>
    ]);
    var options = {
      title: 'Diskusi'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
  }
</script>

</body>
</html>