<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kategori extends CI_Model {

	public function list_dokumen()
	{
		$this->db->select('tbl_kategori_dokumen.nama_kategori, tbl_kategori_dokumen.id_kategori_dokumen')
					->from('tbl_kategori_dokumen')
					->order_by('tbl_kategori_dokumen.id_kategori_dokumen','DESC');
		$list = $this->db->get();

		return $list->result();
	}

	public function save($data)
	{
		$this->db->insert('tbl_kategori_dokumen', $data);
	}

	public function save_blog($data)
	{
		$this->db->insert('tbl_kategori', $data);
	}

	public function delete($id)
	{
		$this->db->delete('tbl_kategori_dokumen', array('id_kategori_dokumen' => $id));
	}

	public function delete_blog($id)
	{
		$this->db->delete('tbl_kategori', array('id_kategori' => $id));
	}

	public function list_blog()
	{
		$this->db->select('tbl_kategori.*')
					->from('tbl_kategori')
					->order_by('tbl_kategori.id_kategori','DESC');
		$list = $this->db->get();

		return $list->result();
	}


}

/* End of file Model_kategori.php */
/* Location: ./application/modules/kategori/models/Model_kategori.php */