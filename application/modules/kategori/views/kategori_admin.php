<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab"><i class="fa fa-book"> <?=$title_dokumen ?></i></a></li>
               
            </ul>
            <div class="tab-content"><br>
              <!-- Font Awesome Icons -->
              <div class="tab-pane active" id="user">
               <?php if($this->session->flashdata('create')):?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo $this->session->flashdata('create'); ?></strong>
                </div>
                <?php elseif($this->session->flashdata('delete')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                    </div>
                <?php elseif($this->session->flashdata('create_blog')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('create_blog'); ?></strong>
                    </div>
                <?php elseif($this->session->flashdata('delete_blog')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('delete_blog'); ?></strong>
                    </div>
                <?php endif; ?>
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th><button class="btn btn-block btn-primary" data-toggle="modal" data-target="#addKategori">
                    <i class="fa fa-plus"></i> Tambah Kategori
                </button></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $i=1;
                    foreach ($list as $result) { ?>
                <tr>
                  <td width="5%"><?=$i++ ?></td>
                  <td><?=$result->nama_kategori?></td>
                  <td width="20%">
                     <a href="<?=base_url('kategori/delete/'.$result->id_kategori_dokumen)?>" class="btn btn-block btn-danger"><i class="btn-icon-only icon-remove">
                        Hapus</i></a>
                  </td>
                </tr>
                <?php } ?>   
                </tbody>
              </table>

            </div>
              <!-- /#fa-icons -->

              <!-- glyphicons-->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->  
    </div>
    </div>
  </section>

  <!-- konten kategori blog -->
    <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab"><i class="fa fa-book"> <?=$title_blog ?></i></a></li>
            </ul>
            <div class="tab-content"><br>
              <!-- Font Awesome Icons -->
              <div class="tab-pane active" id="user">
                <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Keterangan</th>
                  <th><button class="btn btn-block btn-primary" data-toggle="modal" data-target="#addblog">
                    <i class="fa fa-plus"></i> Tambah Kategori
                </button></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $i=1;
                    foreach ($list_blog as $blog) { ?>
                <tr>
                  <td width="5%"><?=$i++ ?></td>
                  <td><?=$blog->nama_kategori?></td>
                  <td><?=$blog->keterangan ?></td>
                  <td width="20%">
                     <a href="<?=base_url('kategori/delete_blog/'.$blog->id_kategori)?>" class="btn btn-block btn-danger"><i class="btn-icon-only icon-remove">
                        Hapus</i></a>
                  </td>
                </tr>
                <?php } ?>   
                </tbody>
              </table>

            </div>
              <!-- /#fa-icons -->

              <!-- glyphicons-->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->  
    </div>
    </div>
  </section>
</div>

<!-- modal add kriteria -->
<div class="modal fade" id="addKategori" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Kategori Dokumen</h4>
            </div>

            <form id="tambahKriteria" action="<?=base_url()?>kategori/create" class="form-horizontal" method="POST">
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Kategori</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="nama_kategori" placeholder="Nama Kategori">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- end modal add kriteria -->

<!-- modal add kategori blog -->
<div class="modal fade" id="addblog" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Kategori Blog</h4>
            </div>

            <form id="tambahKriteria" action="<?=base_url()?>kategori/create_blog" class="form-horizontal" method="POST">
                <div class="modal-body">
                    
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Nama Kategori</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nama_kategori_blog" placeholder="Nama Kategori">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">Keterangan</label>
                      <div class="col-sm-8">
                        <textarea class="form-control" rows="5" name="keterangan_kategori_blog" placeholder="Keterangan Kategori"></textarea>
                      </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- end modal add kriteria -->