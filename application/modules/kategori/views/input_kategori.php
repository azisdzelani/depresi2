<div class="content-wrapper">
  
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"> <?=$title?></h3>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="container">
              <div class="col-sm-12">
                <form class="form-horizontal" method="POST" action="<?=base_url('berita/do_create') ?>" enctype="multipart/form-data">
                  <div class="box-body">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>

                      <div class="col-sm-8">
                        <input type="text" name="judul" value="<?=set_value('judul'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Judul">
                        <?=form_error('judul') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Kategori</label>

                      <div class="col-sm-8">
                        <select name="kategori" class="form-control">
                          <option value="">--Pilih Kategori--</option>
                            <?php foreach($kategori as $k): ?>
                          <option value="<?=$k->id_kategori_berita ?>"><?=$k->nama_kategori ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?=form_error('kategori') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">isi Berita</label>

                      <div class="col-sm-8">
                        <textarea rows="10" name="isi" id="ckeditor1" class="form-control"><?=set_value('isi'); ?></textarea>
                        <?=form_error('isi') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Pilih Gambar</label>

                      <div class="col-sm-8">
                        <input type="file" name="userfile" class="form-control" id="inputEmail3">
                      </div>
                    </div><br>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label"></label>
                      
                      <div class="col-sm-8">
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-download"></i> Save</button> 
                      </div>
                    </div>

                  </div>
                 </form> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      </div>
    </div>
  </section>

</div>
