<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_kategori'  => 'kategori'));
	}

	public function index()
	{
		$data = array(
				'title_dokumen'		=> 'Daftar Master Kategori Dokumen',
				'title_blog'		=> 'Daftar Master Kategori Blog',
				'list' 				=> $this->kategori->list_dokumen(),
				'list_blog' 		=> $this->kategori->list_blog());
			// echo "<pre>";
  	// 	var_dump($data['list_blog']);

		$this->template->content->view('kategori_admin', $data);
		$this->template->publish('template', array('title'=>'Dokumen'));	
	}

	public function create()
	{
		// table kriteria
		$data = array(
			'nama_kategori'	=> ucwords($this->input->post('nama_kategori'))
			);

		$this->kategori->save($data);
		$this->session->set_flashdata('create','Kategori dokumen berhasil ditambah');
		redirect('kategori');
		
	}

	public function create_blog()
	{
		// table kriteria
		$data = array(
			'nama_kategori'	=> ucwords($this->input->post('nama_kategori_blog')),
			'keterangan'	=> ucwords($this->input->post('keterangan_kategori_blog'))
			);

		$this->kategori->save_blog($data);
		$this->session->set_flashdata('create_blog','Kategori blog berhasil ditambah');
		redirect('kategori');
		
	}



	public function do_create()
	{
		$this->rules();

		if ($this->form_validation->run() == TRUE or FALSE) {
			$this->create();
		} else {
			$config['upload_path']          = './uploads/';
	       	$config['allowed_types']        = 'docx|doc|pdf';
	        $config['max_size']             = '2000';
	        $config['max_width']            = '2000';
	        $config['max_height']           = '2000';

        	$this->load->library('upload', $config);

        	if ( ! $this->upload->do_upload()){	

         	//file gagal di upload -> kembali ke form tambah dokumen
         	$data = array('title' 	 => 'Form Tambah Dokumen',
					  'kategori' => $this->kategori->lists());

			$this->template->content->view('input_dokumen', $data);
			$this->template->publish('template', array('title'=>'Input Dokumen'));
         	}
         	else{
         	// file berhasil di upload -> lanjutkan INSERT
         	$dokumen = $this->upload->data();
         	$tanggal = date("Y-m-d H:i:s");
			$data = array(
			'id_kategori_dokumen'	=> $this->input->post('kategori'),
			'judul' 				=> $this->input->post('judul'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'tanggal'				=> $tanggal,
			'file' 					=> $this->input->post('file'),
			'file'					=> $dokumen['file_name']
			);

			$this->dokumen->save($data);
			redirect('dokumen');
         }
		}


	}

	public function delete($id)
	{

		$this->kategori->delete($id);
		$this->session->set_flashdata('delete','Kategori dokumen berhasil dihapus');

		redirect('kategori');
	}

	public function delete_blog($id)
	{

		$this->kategori->delete_blog($id);
		$this->session->set_flashdata('delete_blog','Kategori blog berhasil dihapus');

		redirect('kategori');
	}
// ============================================ Kategori Blog ==================================================================
	// public function kategori_blog()
	// {
	// 	$data = array(
	// 			'kategori_blog'	=> 'Daftar Kategori Blog',
	// 			'list_blog' 	=> $this->kategori->list_blog());
	// 		echo "<pre>";
 //  		var_dump($data['list_blog']);

	// 	$this->template->content->view('kategori_admin', $data);
	// 	$this->template->publish('template', array('title'=>'Dokumen'));	
	// }
}

/* End of file Kategori.php */
/* Location: ./application/modules/kategori/controllers/Kategori.php */