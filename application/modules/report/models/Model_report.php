<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_report extends CI_Model {

	
// 	public function lists($id)
// 	{

// 		// $list = "SELECT DISTINCT id_kategori, 
// 		// 		(SELECT COUNT(*) 
// 		// 		FROM tbl_diskusi 
// 		// 		WHERE id_kategori = a.id_kategori) as total_per_kategori 
// 		// 		FROM `tbl_diskusi` as a 
// 		// 		WHERE id_pegawai = " . $id;

// 		$this->db->select('id_kategori');
// 		$this->db->distinct();
// 		$this->db->select('(SELECT COUNT(*) FROM tbl_diskusi WHERE id_kategori = a.id_kategori) as total_per_kategori');
// 		$this->db->from('tbl_diskusi as a');
// 		$this->db->where('id_pegawai', $id);
// 		$list = $this->db->get();
		
// 		return $list->result();
// 	}

// 	public function  get_all($id_kategori)
// 	{

// 		$this->db->where('id_kategori', $id_kategori);
// 		$this->db->from('tbl_diskusi');
// 		return $this->db->count_all_results();
// 	}

// 	public function get_per_kategori($id_pegawai, $id_kategori) {
// 		$this->db->where('id_pegawai', $id_pegawai);
// 		$this->db->where('id_kategori', $id_kategori);
// 		$this->db->from('tbl_diskusi');
// 		return $this->db->count_all_results();
// 	}

// 	public function get_by_pegawai()
// 	{
// 		$query = $this->db->select('tbl_diskusi.id_pegawai, tbl_pegawai.id_pegawai, tbl_pegawai.nama_lengkap')
// 				 			->from('tbl_diskusi')
// 				 			->join('tbl_pegawai','tbl_diskusi.id_pegawai = tbl_pegawai.id_pegawai')
// 				 			->group_by('tbl_diskusi.id_pegawai')
// 				 			// ->order_by('tbl_diskusi.id_pegawai', 'DESC')
// 				 			->get();

// 		return $query->result();
// 	}

// 	public function total_artikel_pegawai($id_pegawai)
// 	{
// 		$query = $this->db->select('tbl_diskusi.*')
// 							->from('tbl_diskusi')
// 							->where('id_pegawai', $id_pegawai)
// 							->get();
// 		return $query->num_rows();
// 	}

// 	public function get_artikel_pegawai($id_pegawai)
// 	{
// 		$query = $this->db->select('tbl_diskusi.*, tbl_kategori.*')
// 							->from('tbl_diskusi')
// 							->join('tbl_kategori', 'tbl_diskusi.id_kategori = tbl_kategori.id_kategori')
// 							->group_by('tbl_diskusi.id_kategori')
// 							->where('tbl_diskusi.id_pegawai', $id_pegawai)
// 							->get();
// 		return $query->result();
// 	}

// 	public function total_artikel($id_pegawai)
// 	{
// 		$query = $this->db->select('tbl_diskusi.*')
// 							->from('tbl_diskusi')
// 							->where('id_pegawai', $id_pegawai)
// 							->get();
// 		return $query->num_rows();
// 	}
// // ========================================= report kasubag end =================================================

// // ========================================= report kabag =======================================================
// 	public function get_diskusi()
// 	{
// 		$query = $this->db->select('tbl_diskusi.*, tbl_kategori.*')
// 							->from('tbl_diskusi')
// 							->join('tbl_kategori', 'tbl_diskusi.id_kategori = tbl_kategori.id_kategori')
// 							->group_by('tbl_diskusi.id_kategori')
// 							->get();
// 		return $query->result();
// 	}

// 	public function total_kategori($id_kategori)
// 	{
// 		$query = $this->db->select('tbl_diskusi.*, tbl_kategori.*')
// 							->from('tbl_diskusi')
// 							->join('tbl_kategori', 'tbl_diskusi.id_kategori = tbl_kategori.id_kategori')
// 							->where('tbl_diskusi.id_kategori', $id_kategori)
// 							->get();
// 		return $query->num_rows();
// 	}

	public function get_total_diskusi($id)
	{
			
			$this->db->select("tbl_pegawai.id_pegawai, tbl_diskusi.id_kategori, tbl_kategori.nama_kategori, count(tbl_diskusi.id_kategori) as jml_dsk, tbl_diskusi.id_kategori");
			$this->db->from('tbl_diskusi');
			// $this->db->join('tbl_diskusi', 'tbl_diskusi.id_diskusi = tbl_komentar.id_diskusi');
			$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_diskusi.id_pegawai');
			$this->db->join('tbl_kategori', 'tbl_diskusi.id_kategori = tbl_kategori.id_kategori');
			$this->db->where('tbl_pegawai.id_pegawai', $id);
			$this->db->group_by('tbl_diskusi.id_kategori');

			$list = $this->db->get();
			return $list->result_array();
	}

	public function lists_pegawai($id_jabatan)
	{
		$this->db->select('tbl_pegawai.id_pegawai, tbl_pegawai.nama_lengkap, tbl_pegawai.nip, tbl_jabatan.nama_jabatan, tbl_pegawai.email, tbl_user.level_user, tbl_user.status');
		$this->db->where('tbl_pegawai.id_jabatan', $id_jabatan);
		$this->db->from('tbl_pegawai');
		$this->db->join('tbl_user', 'tbl_pegawai.id_pegawai = tbl_user.id_pegawai');
		$this->db->join('tbl_jabatan', 'tbl_pegawai.id_jabatan = tbl_jabatan.id_jabatan');
		$this->db->order_by('tbl_pegawai.id_pegawai','desc');
		$list = $this->db->get();
		/*
		$sql="SELECT tbl_pegawai.id_pegawai, tbl_pegawai.nama_lengkap, tbl_pegawai.email, tbl_user.level_user, tbl_user.status 
		      FROM tbl_pegawai
		      JOIN tbl_user USING(id_pegawai)";
		$list = $this->db->query($sql);
		*/
		return $list->result_array();
	}


}

/* End of file Model_report.php */
/* Location: ./application/modules/report/models/Model_report.php */