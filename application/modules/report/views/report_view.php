<div class="content-wrapper">

<section class="content">
  <div class="row">
    <div class="col-sm-12"> 
      <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h1 class="box-title"><i class="fa fa-bars fa-2x"> <?=$title ?></i></h1>
        </div>

        <div class="box-body">
          <div class="container">
            <div class="row">
              <p>chart</p>
              <div class="col-md-10">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <?php foreach ($pegawai as $pgw) :
                      ?>
                      <p>Nama Pegawai&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>&nbsp;<?= $pgw['nama_lengkap'] ?></strong> </p> 
                      <p>NIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>&nbsp;<?= $pgw['nip'] ?></strong></p> 
                      <p>Bagian/Sub Bagian/Sub Bidang         &nbsp;&nbsp;&nbsp;: <strong>&nbsp;<?= $pgw['nama_jabatan'] ?></strong></p>

                      <table class="table table-bordered">
                        <tr class="info">
                          <th style="text-align:center; width: 10px ">No</th>
                          <th style="text-align:center">Nama Kategori</th>
                          <th style="text-align:center">Jumlah Artikel</th>
                        </tr>
                        <?php 
                        $total = 0;
                        foreach ($pgw['total_dsk'] as $total_dsk) {
                          $total += $total_dsk['jml_dsk'];
                          ?>
                          <tr>
                            <td><?php echo $total_dsk['id_kategori'] ?></td>
                            <td><?php echo $total_dsk['nama_kategori'] ?></td>
                            <td style="text-align:center">
                              <span class="badge bg-yellow"><?php echo $total_dsk['jml_dsk'] ?></span>
                            </td>
                          </tr>
                          <?php  
                        } ?>
                        <tr class="active">
                          <th colspan="2">Total Artikel</th>
                          <th style="text-align:center"><span class="label label-primary"><?php echo $total ?></span></th>
                        </tr>
                      </table><hr><br>
                    <?php
                    endforeach; ?>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
