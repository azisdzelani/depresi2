<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
   
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Examples</a></li>
      <li class="active">Blank page</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 

    <div class="box">

            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-comments-o"></i>

              <h3 class="box-title">Chat</h3>

              <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                <div class="btn-group" data-toggle="btn-toggle">
                  <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i>
                  </button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                </div>
              </div>
            </div>
            <div class="box-body chat" id="chat-box">
              <div class="row margin-bottom">
                    <div class="col-sm-6">
                    <?php foreach ($pegawai as $pgw) :
                    ?>
                    <p>ID: <?php echo $pgw['id_pegawai'] ?></p> 
                    <p>Nama Pegawai: <?php echo $pgw['nama_lengkap'] ?></p> 
                    <p>Jabatan: <?php echo $pgw['nama_jabatan'] ?></p> 
                      <table>
                        <tr>
                          <td>Id Kategori Diskusi</td>
                          <td>Nama Kategori</td>
                          <td>Jumlah Diskusi</td>
                        </tr>
                        <?php 
                        $total = 0;
                        foreach ($pgw['total_dsk'] as $total_dsk) {
                          $total += $total_dsk['jml_dsk'];
                          ?>
                          <tr>
                            <td><?php echo $total_dsk['id_kategori'] ?></td>
                            <td><?php echo $total_dsk['nama_kategori'] ?></td>
                            <td><?php echo $total_dsk['jml_dsk'] ?></td>
                          </tr>
                          <?php  
                        } ?>
                        <tr><th colspan="2">Total</th><th><?php echo $total ?></th></tr>
                      </table>
                    <?php
                    endforeach; ?>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                      <div class="row">
                         
                      <div id="piechart" style="width:800px; height:600px;"></div>
                   
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <h1>artikel</h1>
                </div>
              </div>
            </div>
    </div>
  </div>
  </div>
</section>

</div>
