<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_report', 'report');
	}

	public function index()
	{
		//var_dump($this->session->userdata('id_pegawai'));

		$id_pegawai = $this->session->userdata('id_pegawai');

		if ( $this->session->userdata('id_jabatan') == 3 ) 
		{
			$id_jabatan_staff = '4';
		}
		else{
			$id_jabatan_staff = '5';
		}

		// $data = array(
		// // // 		'title'				  => 'Daftar Laporan Pegawai',
		// // // 		'report'			  => $this->report->get_all_kategori($id_pegawai),
		// 		'report_per_kategori' => $this->report->lists($id_pegawai));
		// // 		'get_by_pegawai' 	  => $this->report->get_by_pegawai());
		// echo "<pre>";
  //       var_dump($data['report_per_kategori']);
		// $data['report'] = $this->report->get_by_pegawai();
		// $data['diskusi'] = $this->report->get_diskusi();
		// var_dump($);
		$data['title'] = 'Laporan Artikel Pegawai';

		$data['pegawai'] = $this->report->lists_pegawai($id_jabatan_staff); 
		for ($i=0; $i<count($data['pegawai']); $i++) {
			$data['pegawai'][$i]['total_dsk'] = $this->report->get_total_diskusi($data['pegawai'][$i]['id_pegawai']);
			//$data['pegawai'][$i]['total_dsk'] = $i;
		}

		// echo "<pre>";
        //print_r($data['pegawai']);die();

		$this->template->content->view('report_view', $data);
		$this->template->publish('template', array('title'=>'Laporan'));	
	}

// ============================= kasubag report =================================================== //

	public function report_kabag($id_kategori = NULL)
	{
		$data['title'] = 'halaman kabag';
		$data['diskusi'] = $this->report->get_diskusi();
		$this->template->content->view('report_kabag', $data);
		$this->template->publish('template', array('title'=>'Laporan'));	
	}


}

/* End of file Report.php */
/* Location: ./application/modules/report/controllers/Report.php */