<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array( 'model_blog' 	   => 'blog',
								  'model_komentar' => 'komentar'));
	}

	public function index()
	{
		$data = array('title' => 'Blog',
					  'list_blog' => $this->blog->lists());

		// echo "</pre>";
  //       var_dump($data['list_blog']);die();
				
		$this->template->content->view('blog_user', $data);
		$this->template->publish('template', array('title'=>'Discussion'));
	}

	public function get_detail_blog($id)
	{
		$id = $this->uri->segment(3); //tangkep param yag dilempar dari url

        $data = array('title' 		  => 'Detail Blog',
        		      'detail_blog' => $this->blog->get_blog_detail($id));
		
	
		$data['komentar'] = $this->blog->get_comment($id);
		
		// echo "</pre>";
  // 		print_r($data);die();

		$this->template->content->view('detail_blog', $data);
		$this->template->publish('template', array('title'=>'Discussion'));
	}

	public function do_create($id)
	{
		
		$data_comment = array(
						'id_blog'	 	=> $this->input->post('id_blog'),
						'isi_komentar' 	=> $this->input->post('komen'),
						'date'			=> date('Y-m-d H:i:s'),
						'id_pegawai'	=> $this->session->userdata('id_pegawai')
			);

		$this->blog->save($data_comment);
		$this->session->set_flashdata('delete_diskusi','Dokumen berhasil ditambah');
		redirect('blog/get_detail_blog/'.$id);
	}

	public function create_blog()
	{
		$data = array('title' => 'Tambah Diskusi');

		// echo "</pre>";
  		//print_r($data['list_blog']);die();
				
		$this->template->content->view('input_blog', $data);
		$this->template->publish('template', array('title'=>'Blog User'));
	}

	public function do_create_blog()
	{

		$data_blog = array(
						'judul_blog' 	=> $this->input->post('judul_blog'),
						'isi_blog' 		=> $this->input->post('isi_blog'),
						'date'			=> date('Y-m-d H:i:s'),
						'id_pegawai'	=> $this->session->userdata('id_pegawai')
			);


		$this->blog->simpan($data_blog);
		$this->session->set_flashdata('create_kriteria','Diskusi berhasil ditambah');
		redirect('blog');
	}

	public function rules()
	{
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('nip', 'NIP', 'required|numeric|max_length[22]');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_error_delimiters('<p style="color:red;">*','</p>');

	}

	public function delete($id)
	{

		$this->blog->delete($id);
		$this->session->set_flashdata('delete','Diskusi berhasil dihapus');

		redirect('blog');
	}

}

/* End of file Blog.php */
/* Location: ./application/modules/blog/controllers/Blog.php */