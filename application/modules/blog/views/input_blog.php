<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><?=$title ?></h3>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="container">
              <div class="col-sm-12">
                <form class="form-horizontal" method="POST" action="<?=base_url('blog/do_create_blog') ?>">
                  <div class="box-body">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Pertanyaan</label>

                      <div class="col-sm-6">
                        <input type="text" name="judul_blog" value="<?=set_value('judul'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Pertanyaan Anda" required>
                        <?=form_error('judul') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Isi Diskusi</label>

                      <div class="col-sm-6">
                        <textarea rows="5" name="isi_blog" class="form-control" required="" ></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label"></label>

                      <div class="col-sm-6">
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-send"></i> Post</button> 
                      </div>
                    </div>

                  </div>
                 </form> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      </div>
    </div>
  </section>

</div>
