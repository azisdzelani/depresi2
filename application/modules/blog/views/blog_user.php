<div class="content-wrapper">
  
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-comments-o"> Forum Diskusi</i></h3>
          </div><br>
          

          <div class="box-body">
          <?php if($this->session->flashdata('delete_diskusi')):?>
              <div class="alert alert-info">
                  <a href="#" class="close" data-dismiss="alert">&times;</a>
                  <strong><?php echo $this->session->flashdata('delete_diskusi'); ?></strong>
              </div>
          <?php endif; ?>
            <div class="row">
              <div class="col-md-12">
              <?php if($this->session->flashdata('create_kriteria')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('create_kriteria'); ?></strong>
                    </div>
              <?php elseif($this->session->flashdata('delete')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                    </div>
                <?php endif; ?>
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr class="info">
                      <td colspan="2"><a href="<?=base_url()?>blog/create_blog" class="btn btn-primary btn-md"><i class="fa fa-plus">  Buat Diskusi</i></a></td>
                      <td colspan="2">
                        <form action="#" method="post" class="pull-right">
                          <div class="input-group">
                            <input type="hidden" name="id_diskusi" value="">
                            <input type="text" name="komen" class="form-control" placeholder="Cari Topik...">
                              <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-search"></i></button>
                              </span>
                          </div>
                        </form>
                      </td>
                      <!-- admin hapus blog -->
                      <?php 
                      if ($this->session->userdata('level_user') == 'Admin') {
                      ?>
                        <td><center>Aksi</center></td>
                      <?php } ?>
                    <!--  end admin hapus blog -->
                    </tr>
                  </thead>
                  <?php
                  foreach ($list_blog as $result) { ?>
                  <tbody>
                    <tr>
                      <td align="center"><b style="color:blue">[TANYA]</b></td>
                      <td>
                        <a href="<?=base_url('blog/get_detail_blog/'.$result->id_blog)?>" style="color:black"><strong><?=$result->judul_blog?></strong></a>
                        <ul class="list-inline text-muted">
                          <li style="padding-right:10px;"><i class="fa fa-user"> Oleh: <?=$result->nama_lengkap?> </i></li>
                        </ul>
                      </td>
                      <td>
                        <p><i class="fa fa-calendar"> <?=date('d-F-Y', strtotime($result->date)); ?></i></p>
                        <ul class="list-inline text-muted">
                          at <?=date('H:i a', strtotime($result->date)); ?>
                        </ul>
                      </td>
                      <td align="center">
                        <p><i class="fa fa-comment"> <?=$result->komentar?> Jawaban</i></p>
                      </td>
                      <!-- admin hapus blog -->
                      <?php 
                      if ($this->session->userdata('level_user') == 'Admin') {
                      ?>
                        <td>
                          <a href="<?=base_url('blog/delete/'.$result->id_blog)?>" class="btn btn-block btn-danger"><i class="btn-icon-only icon-remove">
                          Hapus</i></a> 
                        </td>
                      <?php } ?>
                    <!--  end admin hapus blog -->
                    </tr>
                  </tbody>
                  <?php } ?>
                </table>
              </div>
            </div>
              
          </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
