<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-widget">
          <div class="box-header with-border">
            <div class="user-block">
              
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <!-- post text -->
              <div class="panel panel-default">
                <div class="panel-body">
                  <span class="username"><h4><?=ucwords($detail_blog->judul_blog) ?></h4></span>
                  <small class="text-muted">
                    <span class="description">Ditanya Oleh: <b><?=$detail_blog->nama_lengkap?></b> , 
                      <?=date('d-F-Y', strtotime($detail_blog->date)); ?> 
                    </span>
                  </small><hr>
                <p><strong><?=$detail_blog->isi_blog ?></strong></p>  
                </div>              
              </div>

            <!-- Social sharing buttons -->
            <button type="button" class="btn btn-success btn-xs"><i class="fa fa-share"></i> Share</button>
            <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
            <span class="pull-right text-muted">45 likes - <?=$detail_blog->komentar ?> comments</span>
          </div>
          <!-- /.box-body -->
          <div class="box-footer box-comments">
            <?php 
            foreach ($komentar as $list) { ?>
            <div class="box-comment">

              <!-- User image -->
              <img class="img-circle img-sm" src="<?=base_url()?>assets/images/user-lrg.png" alt="User Image">

              <div class="comment-text">
                <span class="username">
                  <?=$list->nama_lengkap?> - <small class="text-muted"><?=date('G:i a', strtotime($list->date)); ?></small>
                  <span class="text-muted pull-right"><?=date('d-F-Y', strtotime($list->date)); ?></span>
                </span><!-- /.username -->
                <?=$list->isi_komentar?>
              </div>
              <!-- /.comment-text -->
            </div>
            <?php } ?>
            <!-- /.box-comment -->
          </div><br>
          <!-- /.box-footer -->
          <div class="box-footer">
            <form action="<?=base_url('blog/do_create/'.$detail_blog->id_blog) ?>" method="post">
              <img class="img-responsive img-circle img-sm" src="<?=base_url() ?>assets/images/comment_add.png" alt="Alt Text">
              <!-- .img-push is used to add margin to elements next to floating images -->
              <div class="img-push">
                <input type="hidden" name="id_blog" value="<?=$detail_blog->id_blog?>">
                <input type="text" name="komen" class="form-control input-sm" placeholder="Press enter to post comment" required>
              </div>
            </form>
          </div>
            <!-- /.box-footer -->
        </div>
      </div>
    </div>
  </section>

</div>
