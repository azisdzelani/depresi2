<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_blog extends CI_Model {

	public function lists()
	{	
		$this->db->select('tbl_blog.*, tbl_pegawai.*, (SELECT count(id_komentar_blog) FROM tbl_komentar_blog WHERE tbl_komentar_blog.id_blog = tbl_blog.id_blog) as komentar');
		$this->db->from('tbl_blog');
		$this->db->join('tbl_pegawai', 'tbl_blog.id_pegawai = tbl_pegawai.id_pegawai');
		$this->db->order_by('tbl_blog.id_blog','desc');
		$list = $this->db->get();

		return $list->result();
	}

	public function get_blog_detail($id)
	{
		$this->db->select('tbl_blog.*, tbl_pegawai.*, (SELECT count(id_komentar_blog) FROM tbl_komentar_blog WHERE tbl_komentar_blog.id_blog = tbl_blog.id_blog) as komentar');
		$this->db->from('tbl_blog');
		$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_blog.id_pegawai');
		$this->db->where('id_blog', $id);

		$list = $this->db->get();
		return $list->row();
	}

	public function get_comment($id)
	{
		$this->db->select('tbl_komentar_blog.*, tbl_pegawai.id_pegawai, tbl_pegawai.nama_lengkap');
		$this->db->from('tbl_komentar_blog');
		// $this->db->join('tbl_diskusi', 'tbl_diskusi.id_diskusi = tbl_komentar.id_diskusi');
		$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_komentar_blog.id_pegawai', 'left');
		$this->db->order_by('tbl_komentar_blog.id_komentar_blog');
		$this->db->where('tbl_komentar_blog.id_blog', $id);

		$list = $this->db->get();
		return $list->result();
	}

	public function save($data_comment)
	{
		$this->db->insert('tbl_komentar_blog', $data_comment);

	}

	public function simpan($data_blog)
	{
		$this->db->insert('tbl_blog', $data_blog);

	}

	public function delete($id)
	{
		$this->db->delete('tbl_blog', array('id_blog' => $id));
	}

}

/* End of file Model_blog.php */
/* Location: ./application/modules/blog/models/Model_blog.php */