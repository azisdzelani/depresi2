<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab"><i class="fa fa-user"> Data Master User</i></a></li>
            </ul>
            <div class="tab-content">
            <?php if($this->session->flashdata('create')):?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo $this->session->flashdata('create'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('update')):?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo $this->session->flashdata('update'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('delete')):?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                </div>
            <?php endif; ?>

              <!-- Font Awesome Icons -->
              <div class="tab-pane active" id="user"><br>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Level User</th>
                    <th>Status</th>
                    <th><a class="btn btn-large btn-primary" href="<?=base_url('user/create') ?>"><i class="fa fa-plus-square"> Tambah Pegawai</i></a></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                      $i=0;
                      foreach ($lists as $user) { ?>
                  <tr>
                    <td><?=++$i?></td>
                    <td><?=$user->nama_lengkap?></td>
                    <td><?=$user->nip?></td>
                    <td><?=$user->level_user?></span></td>
                    <td><?=$user->status?></span></td>
                    <td width="10%">
                      <a href="<?=base_url('user/edit/'.$user->id_pegawai)?>" class="btn btn-small btn-info"><i class="btn-icon-only icon-pencil">
                        Edit</i></a>

                      <a href="<?=base_url('user/delete/'.$user->id_pegawai)?>" class="btn btn-small btn-danger"><i class="btn-icon-only icon-remove">
                        Hapus</i></a>
                    </td>
                  </tr>
                  <?php } ?>   
                  </tbody>
                </table>
              </div>
              <!-- /#fa-icons -->
              <!-- /#ion-icons -->

            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->  
    </div>
    </div>
  </section>
</div>


