<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?=$title ?></h3>
          </div><br>
          <!-- /.box-header -->
          <!-- form start -->
              <!-- <div><?=validation_errors() ?></div> -->
              <form class="form-horizontal" method="POST" action="<?=base_url('user/do_create') ?>">
                <div class="box-body">

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>

                    <div class="col-sm-6">
                      <input type="text" name="nama" value="<?=set_value('nama'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Nama Lengkap">
                      <?=form_error('nama'); ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>

                    <div class="col-sm-6">
                      <input type="text" name="nip" value="<?=set_value('nip'); ?>"  class="form-control" id="inputEmail3" placeholder="Masukan NIP">
                      <?=form_error('nip'); ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tempat/Tanggal Lahir</label>
                    <div class="row">
                      <div class="col-sm-3">
                        <input type="text" name="tempat_lahir" value="<?=set_value('tempat_lahir'); ?>"  class="form-control" id="inputEmail3" placeholder="Tempat Lahir">
                        <?=form_error('tempat_lahir'); ?>
                      </div>
                      <div class="col-sm-2">
                        <input type="date" name="tanggal_lahir" value="<?=set_value('tanggal_lahir'); ?>"  class="form-control" id="inputEmail3">
                        <?=form_error('tanggal_lahir'); ?>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Kelamin</label>

                    <div class="col-sm-6">
                      <select name="jenis_kelamin" class="form-control">
                        <option value="">--Jenis Kelamin--</option>
                        <option value="laki-laki">Laki-laki</option>
                        <option value="perempuan">Perempuan</option>
                      </select>
                      <?=form_error('jenis_kelamin') ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>

                    <div class="col-sm-6">
                      <select name="agama" class="form-control">
                        <option value="">Pilih Agama</option>
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="protestan">Protestan</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                      </select>
                      <?=form_error('agama') ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>

                    <div class="col-sm-6">
                      <select name="jabatan" class="form-control">
                        <option value="">Pilih Jabatan</option>
                        <?php foreach($jabatan as $result): ?>
                          <option value="<?=$result->id_jabatan ?>"><?=$result->nama_jabatan?></option>
                        <?php endforeach; ?>
                      </select>
                      <?=form_error('jabatan') ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-6">
                      <input type="text" name="email" value="<?=set_value('email'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Email">
                      <?=form_error('email') ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>

                    <div class="col-sm-6">
                      <textarea class="form-control" name="alamat" rows="5">
                        <?=set_value('alamat'); ?>
                      </textarea>
                      <?=form_error('alamat') ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>

                    <div class="col-sm-6">
                      <input type="text" name="telepon" value="<?=set_value('telepon'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Nomer Telepon">
                      <?=form_error('telepon') ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Level User</label>

                    <div class="col-sm-6">
                      <select name="level_user" class="form-control">
                        <option value="" readonly>-- Pilih Level User --</option>
                        <option value="Kepala Bagian Humas">Kepala Bagian Humas</option>
                        <option value="Kepala Sub Bagian">Kepala Sub Bagian</option>
                        <option value="Staff Publikasi">Staff Publikasi</option>
                        <option value="Staff IT">Staff IT</option>
                      </select>
                      <?=form_error('level_user') ?>
                    </div>
                  </div><br>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"></label>
                    <div class="col-sm-6" style="">
                      <button type="submit" class="btn btn-info btn-block"><i class="fa fa-download"> Save</i></button>                    
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
                <!-- <div class="box-footer" style="padding-right:400px;">
                  <button type="submit" class="btn btn-default pull-right">Cancel</button>&nbsp; 
                  <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
              <!-- /.box-footer --> -->
            </form>
          </div>
        </div>
        <!-- /.box -->
  </section>

</div>
