<div class="content-wrapper">
  
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?=$title ?></h3>
          </div><br>
          <!-- /.box-header -->
          <!-- form start -->
              <form class="form-horizontal" method="POST" action="<?=base_url('user/update') ?>">
                <div class="box-body">

                  <input type="hidden" name="id_pegawai" value="<?= $user->id_pegawai ?>">

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>

                    <div class="col-sm-6">
                      <input type="text" name="nama" value="<?=$user->nama_lengkap?>" class="form-control" id="inputEmail3" placeholder="Masukan Nama Lengkap">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>

                    <div class="col-sm-6">
                      <input type="text" name="nip" value="<?=$user->nip?>"  class="form-control" id="inputEmail3" placeholder="Masukan NIP">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tempat/Tanggal Lahir</label>
                    <div class="row">
                      <div class="col-sm-3">
                        <input type="text" name="tempat_lahir" value="<?=$user->tempat_lahir?>"  class="form-control" id="inputEmail3" placeholder="Tempat Lahir">
                      </div>
                      <div class="col-sm-2">
                        <input type="date" name="tanggal_lahir" value="<?=$user->tgl_lahir?>"  class="form-control" id="inputEmail3">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Kelamin</label>

                    <div class="col-sm-6">
                      <select name="jenis_kelamin" class="form-control">
                        <option value="laki-laki"<?=$user->jenis_kelamin=='laki-laki'?' selected':''?>>Laki-laki</option>
                        <option value="perempuan"<?=$user->jenis_kelamin=='perempuan'?' selected':''?>>Perempuan</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>

                    <div class="col-sm-6">
                      <select name="agama" class="form-control">
                        <option value="islam"<?=$user->agama=='islam'?' selected':''?>>Islam</option>
                        <option value="kristen"<?=$user->agama=='kristen'?' selected':''?>>Kristen</option>
                        <option value="protestan"<?=$user->agama=='protestan'?' selected':''?>>Protestan</option>
                        <option value="hindu"<?=$user->agama=='hindu'?' selected':''?>>Hindu</option>
                        <option value="budha"<?=$user->agama=='budha'?' selected':''?>>Budha</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>

                    <div class="col-sm-6">
                      <select name="jabatan" required class="form-control">
                        <option>Pilih Salah Satu</option>
                        <?php
                        foreach ($jabatan as $j) {
                         echo "<option value='{$j->id_jabatan}'".($j->id_jabatan==$user->id_jabatan?' selected':'').">{$j->nama_jabatan}</option>";
                       } 

                       ?>
                     </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="email" value="<?=$user->email?>" placeholder="Masukan Email">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>

                    <div class="col-sm-6">
                      <textarea rows="5" name="alamat" class="form-control"><?=$user->alamat_rumah?></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>

                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="telepon" value="<?=$user->telepon?>" placeholder="Masukan Nomer Telepon">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status</label>

                    <div class="col-sm-6">
                      <select name="status" class="form-control">
                        <option value="Aktif"<?=$user->status=='Aktif'?' selected':''?>>Aktif</option>
                        <option value="Tidak Aktif"<?=$user->status=='Tidak Aktif'?' selected':''?>>Tidak Aktif</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Level User</label>

                    <div class="col-sm-6">
                      <select name="level_user" class="form-control">
                        <option value="" readonly>--Level User--</option>
                        <option value="Kepala Bagian Humas"<?=$user->level_user=='Kepala Bagian Humas'?' selected':''?>>Kepala Bagian Humas</option>
                        <option value="Kepala Sub Bagian"<?=$user->level_user=='Kepala Sub Bagian'?' selected':''?>>Kepala Sub Bagian</option>
                        <option value="Staff Publikasi"<?=$user->level_user=='Staff Publikasi'?' selected':''?>>Staff Publikasi</option>
                        <option value="Staff IT"<?=$user->level_user=='Staff IT'?' selected':''?>>Staff IT</option>
                      </select>
                    </div>
                  </div>


                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"></label>
                    <div class="col-sm-6" style="">
                      <button type="submit" class="btn btn-info btn-block"><i class="fa fa-download"> Update</i></button>                    
                    </div>
                  </div>

                </div>
                <!-- /.box-body -->
              <!--   <div class="box-footer">
                  <button type="submit" class="btn btn-default pull-right">Cancel</button>
                  <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
              <!-- /.box-footer --> -->
            </form>
          </div>
        </div>
        <!-- /.box -->
  </section>

</div>
