<div class="content-wrapper">
  
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <div class="box box-success box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Ubah Password</h3>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <?php if($this->session->flashdata('update')):?>
              <div class="alert alert-info">
                  <a href="#" class="close" data-dismiss="alert">&times;</a>
                  <strong><?php echo $this->session->flashdata('update'); ?></strong>
              </div>
          <?php endif; ?>

            <form class="form-horizontal" method="post" action="<?=base_url('user/ubah_password') ?>">

              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Password Lama</label>

                  <div class="col-sm-6">
                    <input type="text" name="pass_lama" class="form-control" value="<?= set_value('pass_lama') ?>" id="inputEmail3">
                  </div>
                  <?=form_error('pass_lama','<span class="text-red">','</span>')?>
                  <span class="help-block"><?php echo $this->session->flashdata('error'); ?></span>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password Baru</label>

                  <div class="col-sm-6">
                    <input type="password" name="pass_baru" class="form-control" id="inputPassword3">
                  </div>
                  <?=form_error('pass_baru','<span class="text-red">','</span>')?>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Ulangi Password Baru</label>

                  <div class="col-sm-6">
                    <input type="password" name="ulangi_pass" class="form-control" id="inputPassword3">
                  </div>
                  <?=form_error('ulangi_pass','<span class="text-red">','</span>')?>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary btn-small btn-block"><i class="fa fa-download"></i> Simpan</button>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /.box -->
  </section>
</div>
