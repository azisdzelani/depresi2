<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-body">
            <div class="row">
              <!-- kolom kanan diskusi  -->
              <div class="col-sm-12">
              <?php if($this->session->flashdata('create_kriteria')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('create_kriteria'); ?></strong>
                    </div>
              <?php elseif($this->session->flashdata('delete')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                    </div>
              <?php endif; ?>
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr class="active">
                      <td colspan="3">
                        <h1 class="box-title"><i class="fa fa-list"> <?=strtoupper($kategori->nama_kategori) ?></i></h1>
                        <p><?=$kategori->keterangan ?></p>
                      </td>
                      <!-- admin hapus blog -->
                      <?php 
                      if ($this->session->userdata('level_user') == 'Admin') {
                      ?>
                        <td>
                          
                        </td>
                      <?php } ?>
                    <!--  end admin hapus blog -->
                    </tr>
                    <tr class="info">
                      <td colspan="2">
                        <form action="#" method="post" class="pull-left">
                          <div class="input-group">
                            <input type="hidden" name="id_diskusi" value="">
                            <input type="text" name="komen" class="form-control" placeholder="Cari Topik...">
                              <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-search"></i></button>
                              </span>
                          </div>
                        </form>
                      </td>
                      <td>
                        <a href="<?=base_url()?>discussion/create_discussion/<?=$this->uri->segment(3)?>" class="btn btn-primary btn-md btn-block pull-right"><i class="fa fa-pencil">  Buat Blog</i></a> 
                      </td>
                      <!-- admin hapus blog -->
                      <?php 
                      if ($this->session->userdata('level_user') == 'Admin') {
                      ?>
                      <td width="10px"><center>Aksi</center></td>
                      <?php } ?>
                    <!--  end admin hapus blog -->
                    </tr>
                  </thead>
                  <?php 
                  foreach ($list_kategori as $result) { ?>
                  <tbody>
                    <tr class="online">
                      <td width="10%">
                        <img src="<?=base_url()?>assets/images/articles_lb.png" style="padding-top:20px;padding-left:10px;width: 50px;height: 50px">
                      </td>
                      <td>
                        <a href="<?=base_url('discussion/detail_by_id/'.$result->id_diskusi)?>" class="text-red"><h4><?=$result->judul_diskusi?></h4></a>
                        <p><i class="fa fa-user"></i> Penulis: <b><?=$result->nama_lengkap?></b></p>
                        <ul class="list-inline text-muted">
                          <li style="padding-right:10px;"><i class="fa fa-calendar"> <?=date('d-F-Y', strtotime($result->tanggal)); ?> </i>, <em><?=date('H:i a', strtotime($result->tanggal)); ?></em></li>
                        </ul>  
                      </td>
                      <td>
                        <center><h1><?=$result->komentar?></h1>
                        <P>Komentar</P></center>
                      </td>
                      <!-- admin hapus blog -->
                      <?php 
                      if ($this->session->userdata('level_user') == 'Admin') {
                      ?>
                        <td>
                          <a href="<?=base_url('discussion/delete/'.$result->id_diskusi)?>" class="btn btn-block btn-danger"><i class="btn-icon-only icon-remove">
                          Hapus</i></a> 
                        </td>
                      <?php } ?>
                    <!--  end admin hapus blog -->
                    </tr>
                  </tbody>
                  <?php } ?>
                </table>
              </div>
            </div>

          </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
