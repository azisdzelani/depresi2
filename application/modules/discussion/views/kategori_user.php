<div class="content-wrapper">
  
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-header with-border">
            <h1 class="box-title"><i class="fa fa-pencil"> <?=$title ?></i></h1>
          </div>

          <div class="box-body">
          <?php if($this->session->flashdata('delete')):?>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                    </div>
          <?php endif; ?>

            <div class="row">
              <div class="col-md-12">
                <button type="button" class="btn btn-flat btn-primary btn-block"><i class="ion ion-fireball"></i> Pilih Katagori</button>
              </div>
            </div><br>

            <div class="row">
              <?php 
              foreach ($lists as $result) { ?>
              <div class="col-md-3">
                <div class="thumbnail">
                  <div class="caption">
                    <h3><?=$result->nama_kategori?></h3>
                    <p><?=$result->keterangan?></p>
                    <a class="btn btn-primary" href="<?=base_url('discussion/get_by_kategori/'.$result->id_kategori)?>" class="name">
                      Lihat Sekarang
                    </a>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
