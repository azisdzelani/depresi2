<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?=$title ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Examples</a></li>
      <li class="active">Blank page</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
          <div class="box">

            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-comments-o"></i>

              <h3 class="box-title">Chat</h3>

              <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                <div class="btn-group" data-toggle="btn-toggle">
                  <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i>
                  </button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                </div>
              </div>
            </div>
            <div class="box-body chat" id="chat-box">
           
              <!-- chat item -->
               <?php 
              foreach ($lists as $result) { ?>
              <div class="item">
                <img src="dist/img/user3-128x128.jpg" alt="user image" class="offline">

                <p class="message">
                  <a href="<?=base_url('discussion/get_by_kategori/'.$result->id_kategori)?>" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                    <?=$result->nama_kategori?>
                  </a>
                  <?=$result->keterangan?>
                </p>
              </div>
              <?php } ?>
              <!-- /.item -->
              <!-- /.item -->
            </div>
            <!-- /.chat -->
          </div>        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
