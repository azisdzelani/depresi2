<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
          <div class="box box-widget"">

            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="<?=base_url() ?>assets/images/user-lrg.png" alt="User Image">
                <span class="username"><a href="#"> <?=$list_kategori->nama_lengkap ?></a></span>
                <span class="description"><?=$list_kategori->nama_jabatan ?> - <em><?=date('H:i a', strtotime($list_kategori->tanggal)); ?></em>
                <p class="pull-right"><?=date('d F Y', strtotime($list_kategori->tanggal)); ?></p>
                </span>

              </div>
              <!-- /.user-block -->
            
            </div>
            <!-- /.box-header -->
            <div class="box-body chat" id="chat-box">
              
                  <!-- post text -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <p class="page-header"><?=$list_kategori->judul_diskusi ?></p>
                    <?=$list_kategori->isi_diskusi ?>
                    </div>                      
                  </div>
                  

                  <!-- Attachment -->
                  <!-- <div class="attachment-block clearfix">
                    <img class="attachment-img" src="../dist/img/photo1.png" alt="Attachment Image">

                    <div class="attachment-pushed">
                      <h4 class="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4>

                      <div class="attachment-text">
                        Description about the attachment can be placed here.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a>
                      </div>
                    </div>
                  </div> -->
                  <!-- /.attachment-block -->

                  <!-- Social sharing buttons -->
                  <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
                  <span class="pull-right text-muted"><?=$this->cart->total_items(); ?> Likes</span>
                <!-- /.box-body -->

            </div>
            <div class="box box-widget box-primary direct-chat direct-chat-primary">
              
              
              <div class="box-footer">
              <!-- Conversations are loaded here -->
                <div class="direct-chat-messages">
                  <?php  
                  foreach ($komentar as $list) { ?>
                  <!-- Message. Default to the left -->
                  <?php if ($list->id_pegawai == $list_kategori->id_pegawai) {
                    # co
                  ?>
                    <!-- Message to the right -->
                  <div class="direct-chat-msg right">
                    <div class="direct-chat-info clearfix">
                      <span class="direct-chat-name pull-right"><?=$list->nama_lengkap?></span>
                      <span class="direct-chat-timestamp pull-left"><?=date('H:i a', strtotime($list->date)); ?>  - <em><?=date('d F Y', strtotime($list->date)); ?></em></span>
                    </div>
                    <!-- /.direct-chat-info -->
                    <img class="direct-chat-img" src="<?=base_url()?>assets/images/user-lrg.png" alt="message user image"><!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                      <?=$list->isi_komentar?>
                    </div>
                    <!-- /.direct-chat-text -->
                  </div>
                  <!-- /.direct-chat-msg -->

                  <?php } 

                  else { ?> 

                  <div class="direct-chat-msg">
                    <div class="direct-chat-info clearfix">
                      <span class="direct-chat-name pull-left"><?=$list->nama_lengkap?> - <em class="text-muted"><?=date('h:i a', strtotime($list->date)); ?></em> </span>
                      <span class="direct-chat-timestamp pull-right"><?=date('d F Y', strtotime($list->date)); ?></span>
                    </div>
                    <!-- /.direct-chat-info -->
                    <img class="direct-chat-img" src="<?=base_url()?>assets/images/einstein.jpg" alt="message user image"><!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                      <?=$list->isi_komentar?>
                    </div>
                    <!-- /.direct-chat-text -->
                  </div>
                  <!-- /.direct-chat-msg -->

                  <?php } } ?>


                </div>
                <!--/.direct-chat-messages-->

               
              </div>
            
              <!-- /.box-footer -->
            </div>

            <div class="box-footer">
              <form action="<?=base_url('discussion/do_create/'.$list_kategori->id_diskusi) ?>" method="post">
                <div class="input-group">
                  <input type="hidden" name="id_diskusi" value="<?=$list_kategori->id_diskusi?>">
                  <input type="text" name="komen" placeholder="Type Message ..." class="form-control" required>
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-primary btn-flat" required><i class="fa fa-send"></i> Send</button>
                    </span>
                </div>
              </form>
            </div>
            <!-- /.chat -->
          </div>        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
