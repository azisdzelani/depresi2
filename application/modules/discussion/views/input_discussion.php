<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title">Post di Kategori: <span class="text-red"><?=$kategori_diskusi->nama_kategori ?></span></h3>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="container">
              <div class="col-sm-12">
                <form class="form-horizontal" method="POST" action="<?=base_url('discussion/do_create_discussion') ?>">
                  <div class="box-body">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label">Judul</label>

                      <div class="col-sm-10">
                        <input type="text" name="judul" value="<?=set_value('judul'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Judul Artikel" required>
                        <?=form_error('judul') ?>
                      </div>
                    </div>

                    <input type="hidden" name="id_kategori" value="<?=$this->uri->segment(3) ?>">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label">Isi Blog</label>

                      <div class="col-sm-10">
                        <textarea rows="10" id="ckeditor1" name="isi_diskusi" class="form-control" required="" ></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label"></label>

                      <div class="col-sm-10">
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-send"></i> Post</button> 
                      </div>
                    </div>

                  </div>
                 </form> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      </div>
    </div>
  </section>

</div>
