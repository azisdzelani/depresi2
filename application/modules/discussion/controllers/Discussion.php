<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discussion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array( 'model_discussion' => 'diskusi',
								  'model_komentar' 	 => 'komentar',
								  'model_kategori'	 => 'kategori'));
	}

	public function index()
	{
		$data = array('title' => 'Blog',
					  'lists' => $this->kategori->lists());
				
		$this->template->content->view('kategori_user', $data);
		$this->template->publish('template', array('title'=>'Blog'));		
	}

	public function get_by_kategori($id_kategori)
    {
        $id = $this->uri->segment(3); //tangkep param yag dilempar dari url

        $data = array('title' 		  => 'Blog',
        			  'kategori'	  => $this->diskusi->get_kategori($id_kategori),
        		      'list_kategori' => $this->diskusi->get_by_kategori($id));
        // echo "<pre>";
        // var_dump($data['kategori']);
       
        $this->template->content->view('discussion_user', $data);
        $this->template->publish('template', array('title'=>'Blog')); 
    }

	public function detail_by_id($id)
	{
		
		$id = $this->uri->segment(3); //tangkep param yag dilempar dari url

        $data = array('title' 		  => 'Detail diskusi',
        		      'list_kategori' => $this->diskusi->get_discussion_detail($id));
		
	
		$data['komentar'] = $this->diskusi->get_comment($id);
		
		// echo "</pre>";
  // 		print_r($data['list_kategori']);die;

		$this->template->content->view('detail_discussion', $data);
		$this->template->publish('template', array('title'=>'Blog'));
	}

	public function do_create($id)
	{
		
		$data_comment = array(
						'id_diskusi' 	=> $this->input->post('id_diskusi'),
						'isi_komentar' 	=> $this->input->post('komen'),
						'date'			=> date('Y-m-d H:i:s'),
						'id_pegawai'	=> $this->session->userdata('id_pegawai')
			);

		$this->diskusi->save($data_comment);
		redirect('discussion/detail_by_id/'.$id);
	}

	public function create_discussion($id_kategori)
	{
		
        $data = array('title' 	 => 'Detail diskusi',
        			  'kategori_diskusi' => $this->diskusi->get_kategori($id_kategori));
		 // echo "<pre>";
   //      var_dump($data['kategori']);die();

		$this->template->content->view('input_discussion', $data);
		$this->template->publish('template', array('title'=>'Discussion'));
		
	}

	public function do_create_discussion()
	{

		$data_discussion = array(
						'id_kategori' 	=> $this->input->post('id_kategori'),
						'judul_diskusi' => $this->input->post('judul'),
						'isi_diskusi' 	=> $this->input->post('isi_diskusi'),
						'tanggal'		=> date('Y-m-d H:i:s'),
						'id_pegawai'	=> $this->session->userdata('id_pegawai')
			);

		$id_kategori = $this->input->post('id_kategori');

		$this->diskusi->simpan($data_discussion);
		$this->session->set_flashdata('create_kriteria','Artikel berhasil ditambah');
		redirect('discussion/get_by_kategori/'.$id_kategori);
	}


	public function delete($id)
	{

		$this->diskusi->delete($id);
		$this->session->set_flashdata('delete','Blog berhasil dihapus');

		redirect('discussion');
	}

	

}

/* End of file Discussion.php */
/* Location: ./application/modules/discussion/controllers/Discussion.php */