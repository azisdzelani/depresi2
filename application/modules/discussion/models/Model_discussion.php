<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_discussion extends CI_Model {

		public function lists()
		{
			$this->db->order_by('tanggal', 'DESC');
			$list = $this->db->get('tbl_diskusi');
	
			return $list->result();
		}

		public function get_by_kategori($id)
		{

			$this->db->select('tbl_diskusi.*,tbl_pegawai.id_pegawai, tbl_pegawai.nama_lengkap, (SELECT count(id_komentar) FROM tbl_komentar WHERE tbl_komentar.id_diskusi = tbl_diskusi.id_diskusi) as komentar');
			$this->db->from('tbl_diskusi');
			$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_diskusi.id_pegawai');
			$this->db->where('id_kategori', $id);
			$this->db->order_by('tanggal', 'DESC');
			$list = $this->db->get();
			
			return $list->result();
		}
		
		public function get_by_id($id)
		{
			$this->db->where('id_diskusi', $id);
			$list = $this->db->get('tbl_diskusi');
			

			return $list->row();
		}

		public function get_discussion_detail($id)
		{
			$this->db->select('tbl_diskusi.*, tbl_pegawai.*, tbl_jabatan.*');
			$this->db->from('tbl_diskusi');
			$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_diskusi.id_pegawai');
			$this->db->join('tbl_jabatan', 'tbl_jabatan.id_jabatan = tbl_pegawai.id_jabatan');
			$this->db->where('id_diskusi', $id);

			$list = $this->db->get();
			return $list->row();
		}

		public function get_comment($id)
		{
			$this->db->select('tbl_komentar.*, tbl_pegawai.id_pegawai, tbl_pegawai.nama_lengkap');
			$this->db->from('tbl_komentar');
			// $this->db->join('tbl_diskusi', 'tbl_diskusi.id_diskusi = tbl_komentar.id_diskusi');
			$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_komentar.id_pegawai', 'left');
			$this->db->where('tbl_komentar.id_diskusi', $id);

			$list = $this->db->get();
			return $list->result();
		}

		public function save($data_comment)
		{
			$this->db->insert('tbl_komentar', $data_comment);
	
		}

		public function simpan($data_discussion)
		{
			$this->db->insert('tbl_diskusi', $data_discussion);
	
		}

		public function get_kategori($id_kategori)
		{
			$this->db->select('tbl_diskusi.*, tbl_kategori.*');
			$this->db->from('tbl_diskusi');
			$this->db->join('tbl_kategori', 'tbl_diskusi.id_kategori = tbl_kategori.id_kategori');
			$this->db->where('tbl_diskusi.id_kategori', $id_kategori);

			$list = $this->db->get();
			return $list->row();
		}

		public function delete($id)
		{
			$this->db->delete('tbl_diskusi', array('id_diskusi' => $id));
		}

		// public function get_total_diskusi($id)
		// {
			
		// 	$this->db->select("tbl_pegawai.nama_lengkap, tbl_jabatan.nama_jabatan, tbl_diskusi.id_diskusi, count(tbl_diskusi.id_diskusi)");
		// 	$this->db->from('tbl_pegawai');
		// 	// $this->db->join('tbl_diskusi', 'tbl_diskusi.id_diskusi = tbl_komentar.id_diskusi');
		// 	$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_diskusi.id_diskusi');
		// 	$this->db->join('tbl_jabatan', 'tbl_pegawai.id_jabatan = tbl_jabatan.id_jabatan');
		// 	$this->db->where('tbl_pegawai.id_pegawai', $id);
		// 	$this->db->group_by('tbl_diskusi.id_kategori');

		// 	$list = $this->db->get();
		// 	return $list->result();
		// }

		/*public function get_total_diskusi_kategori($id)
		{
			
			$this->db->select("tbl_pegawai.nama_lengkap, count(tbl_diskusi.id_diskusi)");
			$this->db->from('tbl_pegawai');
			// $this->db->join('tbl_diskusi', 'tbl_diskusi.id_diskusi = tbl_komentar.id_diskusi');
			$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_diskusi.id_diskusi');
			$this->db->where('tbl_pegawai.id_pegawai', $id);

			$list = $this->db->get();
			return $list->result();
		}*/

}

/* End of file Model_discussion.php */
/* Location: ./application/modules/discussion/models/Model_discussion.php */