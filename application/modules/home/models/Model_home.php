<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_home extends CI_Model {

	public function dokumen_home()
	{
		$this->db->select('tbl_dokumen.*, tbl_kategori_dokumen.*, tbl_user.id_pegawai, tbl_pegawai.*')
					->from('tbl_dokumen')
					->join('tbl_kategori_dokumen', 'tbl_dokumen.id_kategori_dokumen = tbl_kategori_dokumen.id_kategori_dokumen')
					->join('tbl_user', 'tbl_dokumen.id_user = tbl_user.id_user')
					->join('tbl_pegawai', 'tbl_user.id_pegawai = tbl_pegawai.id_pegawai')
					->limit(5)
					->order_by('tbl_dokumen.id_dokumen','desc');
		$list = $this->db->get();

		return $list->result();
	}

	public function berita_home()
	{
		$this->db->select('tbl_berita.*, tbl_kategori_berita.*, tbl_user.id_pegawai, tbl_pegawai.*')
					->from('tbl_berita')
					->join('tbl_kategori_berita', 'tbl_berita.id_kategori_berita = tbl_kategori_berita.id_kategori_berita')
					->join('tbl_user', 'tbl_berita.id_user = tbl_user.id_user')
					->join('tbl_pegawai', 'tbl_user.id_pegawai = tbl_pegawai.id_pegawai')
					->limit(5)
					->order_by('tbl_berita.id_berita','DESC');
		$list = $this->db->get();

		return $list->result();
	}

	public function blog_home()
	{
		$this->db->select('tbl_diskusi.*, tbl_kategori.*, tbl_pegawai.*, (SELECT count(id_komentar) FROM tbl_komentar WHERE tbl_komentar.id_diskusi = tbl_diskusi.id_diskusi) as komentar')
					->from('tbl_diskusi')
					->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_diskusi.id_kategori')
					->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_diskusi.id_pegawai')
					->limit(6)
					->order_by('tbl_diskusi.id_diskusi','DESC');
		$list = $this->db->get();

		return $list->result();
	}	

}

/* End of file Model_home.php */
/* Location: ./application/modules/home/models/Model_home.php */