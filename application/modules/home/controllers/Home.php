<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
								 'model_berita'	  => 'berita',
								 'model_home'	  => 'home',
								 'model_kategori' => 'kategori'));
	}

	public function index()
	{
		$data = array(
				'title'		=> 'Home',
				'blog'		=> $this->home->blog_home(),
				'berita'	=> $this->home->berita_home(),
				'dokumen'	=> $this->home->dokumen_home());
		// echo "<pre>";
  // 		var_dump($data['blog']);die();
		
		$this->template->content->view('home_user', $data);
		$this->template->publish('template', array('title'=>'Home'));	
	}

}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */