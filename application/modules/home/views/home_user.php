<div class="content-wrapper">

  <section class="content">

    <div class="row">
      <div class="col-sm-12">
        <div class="box box-default">
          <div class="box-body">
            <div class="row">
              <div class="col-sm-12">
                <img src="<?=base_url() ?>assets/images/logo_home_lan.png">
                <img src="<?=base_url() ?>assets/images/banner-header-lan.png" class="pull-right"><hr>
                <div class="alert alert-info alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <strong>Selamat Datang di Sistem Manajemen Pengetahuan </strong>, <em>Untuk Keamanan Segera Ganti Password Lama Anda...</em>
                </div>
                <!-- kolom berita -->
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="small-box bg-green">
                        <div class="inner">    
                          <h3>Knowledge</h3>
                          <p>sebagai basis data yang berisi kumpulan pengetahuan dalam bentuk eksplisit, atau yang telah dituangkan ke dalam bentuk digital</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-book"></i>
                        </div>
                        <a href="<?=base_url('dokumen/dokumen_user') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="small-box bg-aqua">
                        <div class="inner">
                          <h3>Discussion</h3>
                          <p>sebagai wadah pegawai untuk berdiskusi segala macam hal yang terkait dengan pekerjaan atau bidang pengetahuan lainnya</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-comments"></i>
                        </div>
                        <a href="<?=base_url('blog') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <!-- small box -->
                      <div class="small-box bg-red">
                        <div class="inner">
                          <h3>Blog</h3>
                          <p>merupakan kumpulan tulisan yang dihasilkan oleh semua pegawai yang memiliki pengalaman serta keilmuan yang berbeda</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-pencil"></i>
                        </div>
                        <a href="<?=base_url('discussion') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="small-box bg-yellow">
                        <div class="inner">
                          <h3>News</h3>
                          <p>Menampilkan informasi atau berita terkini seputar kegiatan dilingkungan Lembaga Administrasi Negara</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-newspaper-o"></i>
                        </div>
                        <a href="<?=base_url('berita') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div><br>

            <div class="row">
              <div class="col-sm-4">
                <table class="table table-hover">
                  <th colspan="2" style="border-bottom: solid;">
                    <a class="pull-right" href="https://members.phpmu.com/files">All</a>
                    <h3 style="margin-top:-5px">Dokumen Baru</h3>
                  </th>
                  <?php foreach($dokumen as $result): ?>
                  <tr>
                    <td>
                      <span>
                        <?= ucwords($result->judul); ?> 
                        <small class="btn btn-warning btn-xs"> <?=$result->nama_kategori ?></small>
                      </span>
                      <small>
                        <ul class="list-inline text-muted" style="">
                        <li style="padding-right:10px;"><i class="fa fa-user"> Oleh:</i> <?=$result->nama_lengkap ?> </i></li>
                        <li><i class="fa fa-calendar"></i> <?=date('d-F-Y', strtotime($result->tanggal)); ?></li>
                        <li><i class="fa fa-clock-o"></i> <?=date('g:i a', strtotime($result->tanggal)); ?></li>
                      </ul>
                      </small>
                    </td>
                    <td>
                      <a class="btn btn-success" href="<?=base_url()?>dokumen/download/<?=$result->file?>"><i class="fa fa-download fa-fw"></i></a>
                    </td>
                  </tr>
                  <?php endforeach; ?> 
                </table> 
              </div>
              <!-- menu dokumen end -->
              <div class="col-sm-4">
                <table class="table table-hover">
                  <th colspan="2" style="border-bottom: solid;">
                    <a class="pull-right" href="https://members.phpmu.com/files">All</a>
                    <h3 style="margin-top:-5px">Berita Baru</h3>
                  </th>
                  <?php foreach($berita as $result): ?>
                  <tr>
                    <td width="55px">
                      <img class="img-circle" style="width:65px; height:59px; border:1px solid #8a8a8a" src="<?=base_url('uploads/'.$result->gambar)?>">
                    </td>
                    <td>
                      [<b style="color:blue"><?=$result->nama_kategori ?></b>] - <a style="color:#000" href="<?=base_url('berita/get_by_id/'.$result->id_berita)?>"><?=$result->judul ?></a> <br>
                      <small>Di Posting Oleh: <i class="fa fa-user fa-fw"></i> <?=$this->session->userdata('level_user') ?> 
                      </small> 
                      <small style="text-transform:capitalize"><a style="color:#000" href="https://members.phpmu.com/members/account/PHP0008922"><i class="fa fa-clock-o fa-fw"></i> <?=date('d F Y', strtotime($result->tanggal)); ?></a></small>
                    </td>
                  </tr>
                  <?php endforeach; ?> 
                </table> 
              </div>
              <!-- menu berita end -->
              <div class="col-sm-4">
                <table class="table table-hover">
                  <th colspan="2" style="border-bottom: solid;">
                    <a class="pull-right" href="https://members.phpmu.com/files">All</a>
                    <h3 style="margin-top:-5px">Artikel Baru</h3>
                  </th>
                  <?php foreach($blog as $result): ?>
                  <tr>
                    <td>
                      <span class="text-red"><a style="color:#000" href="<?=base_url('discussion/detail_by_id/'.$result->id_diskusi)?>">
                        <?=ucwords($result->judul_diskusi) ?>
                      </a></span><small class="btn btn-info btn-xs"> <?=$result->nama_kategori ?></small><br>
                      <small>Penulis: <i class="fa fa-user fa-fw"></i> <?=$result->nama_lengkap ?></small> 
                      <small style="text-transform:capitalize"><i class="fa fa-comment fa-fw"></i> <?=$result->komentar ?></small>
                    </td>
                  </tr>
                  <?php endforeach; ?> 
                </table> 
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>

  </section>
                  
</div>
