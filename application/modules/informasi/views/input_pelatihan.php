<div class="content-wrapper">
  
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"> <?=$title?></h3>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="container">
              <div class="col-sm-12">
                <form class="form-horizontal" method="POST" action="<?=base_url('informasi/do_create') ?>" enctype="multipart/form-data">
                  <div class="box-body">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Nama Pelatihan</label>

                      <div class="col-sm-6">
                        <input type="text" name="nama_pelatihan" value="<?=set_value('nama_pelatihan'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Nama Pelatihan">
                        <?=form_error('nama_pelatihan') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Tempat</label>

                      <div class="col-sm-6">
                        <input type="text" name="tempat" value="<?=set_value('tempat'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Tempat Pelatihan">
                        <?=form_error('tempat') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Mulai</label>

                      <div class="col-sm-6">
                        <input type="date" name="tanggal_mulai" value="<?=set_value('tanggal_mulai'); ?>" class="form-control" id="inputEmail3">
                        <?=form_error('tanggal_mulai') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Selesai</label>

                      <div class="col-sm-6">
                        <input type="date" name="tanggal_selesai" value="<?=set_value('tanggal_selesai'); ?>" class="form-control" id="inputEmail3">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>

                      <div class="col-sm-6">
                        <textarea rows="5" name="deskripsi" class="form-control"><?=set_value('deskripsi'); ?></textarea>
                        <?=form_error('deskripsi') ?>
                      </div>
                    </div>

                    <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Pelatihan</label>

                    <div class="col-sm-6">
                      <select name="status" class="form-control">
                        <option value="">--Jenis Pelatihan--</option>
                        <option value="pelatihan">Pelatihan</option>
                        <option value="workshop">Workshop</option>
                        <option value="seminar">Seminar</option>
                        <option value="pendidikan">Pendidikan</option>
                      </select>
                      <?=form_error('status') ?>
                    </div>
                  </div><br>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label"></label>
                      
                      <div class="col-sm-6">
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-download"></i> Save</button> 
                      </div>
                    </div>

                  </div>
                 </form> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      </div>
    </div>
  </section>

</div>
