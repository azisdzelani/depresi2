<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab"><i class="fa fa-user"> <?=$title ?></i></a></li>
            </ul>
            <div class="tab-content">
              <!-- Font Awesome Icons -->
              <div class="tab-pane active" id="user"><br>
              <?php if($this->session->flashdata('create')):?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo $this->session->flashdata('create'); ?></strong>
                </div>
              <?php elseif($this->session->flashdata('delete')):?>
                  <div class="alert alert-info">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                  </div>
              <?php endif; ?>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Pelatihan</th>
                      <th>Tanggal Mulai</th>
                      <th>Tanggal Selesai</th>
                      <th>Tempat</th>
                      <th>Keterangan</th>
                      <th>Jenis</th>
                      <th><a class="btn btn-block btn-large btn-primary" href="<?=base_url('informasi/create') ?>"><i class="fa fa-plus-square"> Tambah Pelatihan</i></a></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
                    $i=1;
                    foreach ($list as $result) { ?>
                    <tr>
                      <td><?=$i++ ?></td>
                      <td><?=$result->nama_pelatihan ?></td>
                      <td><?=$result->tanggal_mulai ?></td>
                      <td><?=$result->tanggal_selesai ?></span></td>
                      <td><?=$result->tempat ?></span></td>
                      <td><?=$result->deskripsi ?></td>
                      <td><?=$result->status ?></td>
                      <td width="10%">
                        <!-- <a href="<?=base_url('informasi/edit/'.$result->id_pelatihan)?>" class="btn btn-small btn-info"><i class="btn-icon-only icon-pencil">
                          Edit</i></a> -->

                        <a href="<?=base_url('informasi/delete/'.$result->id_pelatihan)?>" class="btn btn-block btn-small btn-danger"><i class="btn-icon-only icon-remove">
                          Hapus</i></a>
                      </td>
                    </tr>
                    <?php } ?>  
                  </tbody>
                </table>
              </div>
              <!-- /#fa-icons -->
              <!-- /#ion-icons -->

            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->  
    </div>
    </div>
  </section>
</div>


