<html>
<head>
  <title>Report Table</title>
  <style type="text/css">
    #outtable{
      padding: 20px;
      border:1px solid #e3e3e3;
      width:800px;
      border-radius: 5px;
    }
 
    .short{
      width: 50px;
    }
 
    .normal{
      width: 200px;
    }
 
    table{
      border-collapse: collapse;
      font-family: arial;
      color:#5E5B5C;
    }
 
    thead th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    }
  </style>
</head>
<body>
          <div style="text-align: center;">
            <img align="center" src="<?=base_url() ?>assets/images/garuda-indonesia.jpg" width="50px" height="50px">            
          </div>
            
          <p style="text-align: center;"><strong>LEMBAGA ADMINISTRASI NEGARA <br>
          REPUBLIK INDONESIA</strong> <br>
          <small>Jl.Veteran No.10, Jakarta Pusat 1010</small></p>
          <hr>

      <h4 align="center"><strong>FORMULIR KEIKUTSERTAAN</strong></h4>
      <p>Saya yang bertanda tangan dibawah ini:</p>
      <table>
        <tr>
          <td><p>Nama/Jabatan</p></td>
          <td><p style="padding-left: 90px">:</p></td>
          <td><p style="padding-left: 10px"><b>Azis Abd Dzaelani</b> - Kepala Bagian Hubungan Masyarakat dan Informasi</p></td>
        </tr>
        <tr>
          <td><p>Alamat Instansi</p></td>
          <td><p style="padding-left: 90px">:</p></td>
          <td><p style="padding-left: 10px">Jl. Veteran No.10 Jakarta Pusat 10110</p></td>
        </tr>
        <tr>
          <td><p>Nomor Telephone</p></td>
          <td><p style="padding-left: 90px">:</p></td>
          <td><p style="padding-left: 10px">021 3455021-24</p></td>
        </tr>
      </table><br>
      <p>Mengusulkan peserta untuk mengikuti <b><em><?=$info->nama_pelatihan ?></em></b> di <b><em><?=$info->tempat ?></em></b> dengan nama sebagai berikut: </p>
    
    <div id="outtable">

      <table>
        <thead>
          <tr>
            <th class="short">No</th>
            <th class="normal">Nama</th>
            <th class="normal">Jabatan</th>
            <th class="normal">No Telephone</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; ?>
          <?php foreach($users as $user): ?>
            <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $user->nama_lengkap ?></td>
            <td><?php echo $user->nama_jabatan ?></td>
            <td><?php echo $user->telepon ?></td>
            </tr>
          <?php $no++; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
	  </div>
</body>
</html>