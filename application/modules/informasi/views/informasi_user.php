<div class="content-wrapper">
 
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-header with-border">
            <h1 class="box-title"><i class="fa fa-newspaper-o"> <?=$title?></i></h1>
          </div>

          <div class="box-body">
            <div class="container">
              <div class="row">

                <div class="col-md-11">
                <?php foreach($list as $result): ?>
                  <ul class="timeline">
                    <!-- timeline time label -->
                    <li class="time-label">
                      <span class="bg-red">
                        <?=ucwords($result->status) ?>
                      </span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                      <div class="timeline-item">
                        <h3 class="timeline-header"> <b><?=ucwords($result->nama_pelatihan) ?></b></h3>

                        <div class="timeline-body">
                          <table id="example2" class="table table-bordered">
                            <tr>
                              <th>Tanggal Mulai</th>
                              <th>Tanggal Selesai</th>
                              <th>Tempat</th>
                              <th>Keterangan</th>
                            </tr>
                            <tr>
                              <td><?=date('d F Y', strtotime($result->tanggal_mulai)); ?></td>
                              <td><?=date('d F Y', strtotime($result->tanggal_selesai)); ?></td>
                              <td><?=$result->tempat ?></td>
                              <td width="30%"><?=$result->deskripsi ?></td>
                            </tr>
                          </table>
                        </div>
                        <div class="timeline-footer">
                          <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target=" #detailPeserta<?=$result->id_pelatihan ?>" title="Detail Peserta">
                          <i class="fa fa-eye"> lihat Peserta</i>
                          </button>
                          <a href="<?=base_url() ?>informasi/pdf/<?=$result->id_pelatihan ?>" class="btn btn-danger btn-sm"><i class="fa fa-print"> Cetak Undangan</i></a>
                        </div>
                      </div>
                    </li>
                    <!-- END timeline item -->
                  </ul>
                <?php endforeach; ?>
                  <!-- END timeline item -->
                </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>

<!-- modal detail peserta pelatihan -->
<?php foreach($list as $result): ?>
<div class="modal fade" id="detailPeserta<?=$result->id_pelatihan?>" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">List Peserta</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                              <tr class="active">
                                <th>No</th>
                                <th>Nama Pegawai</th>
                                <th>Jabatan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $no = 1; 
                                $this->load->model('model_informasi', 'pelatihan');
                                $data = $this->pelatihan->get_peserta($result->id_pelatihan);
                                foreach ($data as $datas):
                                  if ($result->id_pelatihan == $datas->id_pelatihan): 
                                   
                              ?>
                                <tr>
                                <?php
                                  if ($datas->id_pelatihan != $result->id_pelatihan):?>
                                    <p>Peserta Belum Ada</p>
                                  <?php elseif($datas->id_pegawai != NULL): ?> 
                                    <td><?=$no++ ?></td>
                                    <td><?=$datas->nama_lengkap?></td>
                                    <td><?=$datas->nama_jabatan ?></td>
                                  <?php endif; ?>
                                    
                                </tr>
                              <?php endif; endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
            
        </div>
    </div>
</div>
<?php endforeach; ?>