<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_informasi' => 'informasi'));
		
	}

	public function index()
	{
		$data = array(
				'title'		=> 'Informasi Pelatihan',
				'list'		=> $this->informasi->lists());
		// echo "<pre>";
  		//var_dump($data['list']);die();
		
		$this->template->content->view('informasi_user', $data);
		$this->template->publish('template', array('title'=>'Informasi'));	
	}

	public function info_pelatihan()
	{
		$data = array(
				'title'		=> 'Info Pelatihan',
				'list'		=> $this->informasi->info_pelatihan());
		// echo "<pre>";
  // 		var_dump($data['list']);die();
		
		$this->template->content->view('info_pelatihan', $data);
		$this->template->publish('template', array('title'=>'Informasi'));	
	}

	public function create()
	{
		$data = array(
				'title'		=> 'Input Pelatihan',
				'list'		=> $this->informasi->info_pelatihan());
		// echo "<pre>";
  // 		var_dump($data['list']);die();
		
		$this->template->content->view('input_pelatihan', $data);
		$this->template->publish('template', array('title'=>'Informasi'));
	}

	public function do_create()
	{
		$this->rules();

		if ($this->form_validation->run() == FALSE) {
			
			$this->create();

		} else {

			$data_pelatihan = array(
			'nama_pelatihan'	=> $this->input->post('nama_pelatihan'),
			'tanggal_mulai' 	=> $this->input->post('tanggal_mulai'),
			'tanggal_selesai' 	=> $this->input->post('tanggal_selesai'),
			'tempat'    		=> $this->input->post('tempat'),
			'deskripsi' 		=> $this->input->post('deskripsi'),
			'status' 			=> $this->input->post('status')
			);


			$this->informasi->save($data_pelatihan);
			$this->session->set_flashdata('create','Informasi berhasil ditambah');
			
			redirect('informasi/info_pelatihan');
		}

	}

	public function delete($id)
	{
		$this->informasi->delete($id);
		$this->session->set_flashdata('delete','Informasi berhasil dihapus');

		redirect('informasi/info_pelatihan');
	}

	public function rules()
	{
		$this->form_validation->set_rules('nama_pelatihan', 'Nama Pelatihan', 'required');
		$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'required');
		$this->form_validation->set_rules('tempat', 'Tempat', 'required');
		$this->form_validation->set_rules('deskripsi', 'Keterangan', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', '{field} masih kosong silahkan diisi');
		$this->form_validation->set_error_delimiters('<p style="color:red;">*','</p>');

	}

	public function pdf($id)
	{
	     // page info here, db calls, etc.     
	     $data['users']=$this->informasi->get_peserta($id);
	     $data['info'] =$this->informasi->get_informasi($id);
	     /*echo "<pre>";
  		var_dump($data);die();*/
	     

	     //load the view and saved it into $html variable
        $html=$this->load->view('contoh', $data, true);
 
        //this the the PDF filename that user will get to download
        $pdfFilePath = "surat_rekomendasi_pelatihan.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");

	     // or
	     // $data = pdf_create($html, '', false);
	     // write_file('name', $data);
	     //if you want to write it to disk and/or send it as an attachment    
	}

}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */