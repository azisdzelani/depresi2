<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_informasi extends CI_Model {

	public function lists()
	{
		$this->db->select('tbl_pelatihan.*');
		$this->db->from('tbl_pelatihan');
		$this->db->order_by('tbl_pelatihan.id_pelatihan','desc');
		$list = $this->db->get();

		return $list->result();	
	}

	public function get_peserta($id_pelatihan)
	{
		$this->db->select('tbl_pegawai.*, tbl_jabatan.*, tbl_peserta_pelatihan.*');
		$this->db->from('tbl_peserta_pelatihan');
		$this->db->join('tbl_pegawai', 'tbl_pegawai.id_pegawai = tbl_peserta_pelatihan.id_pegawai');
		$this->db->join('tbl_jabatan', 'tbl_jabatan.id_jabatan = tbl_pegawai.id_jabatan');
		$this->db->where('id_pelatihan', $id_pelatihan);
		$list = $this->db->get();

		return $list->result();
	}

	public function get_informasi($id_pelatihan)
	{
		$this->db->select('tbl_pelatihan.*');
		$this->db->from('tbl_pelatihan');
		$this->db->where('id_pelatihan', $id_pelatihan);
		$list = $this->db->get();

		return $list->row();
	}



	public function info_pelatihan()
	{
		$this->db->select('tbl_pelatihan.*');
		$this->db->from('tbl_pelatihan');
		$this->db->order_by('tbl_pelatihan.id_pelatihan','desc');
		$list = $this->db->get();

		return $list->result();		
	}

	public function save($data_pelatihan)
	{
		$this->db->insert('tbl_pelatihan', $data_pelatihan);
		
	}

	public function delete($id)
	{
		$this->db->delete('tbl_pelatihan', array('id_pelatihan' => $id));

	}

	

}

/* End of file Model_informasi.php */
/* Location: ./application/modules/informasi/models/Model_informasi.php */