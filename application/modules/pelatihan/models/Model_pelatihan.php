<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pelatihan extends CI_Model {

	public function lists()
	{	
		$this->db->select('tbl_berita.*, tbl_kategori_berita.nama_kategori');
		$this->db->from('tbl_berita');
		$this->db->join('tbl_kategori_berita', 'tbl_berita.id_kategori_berita = tbl_kategori_berita.id_kategori_berita');
		$this->db->order_by('tbl_berita.id_berita','desc');
		$list = $this->db->get();

		return $list->result();
	}

	public function save($data)
	{		
		$id_user = $this->session->userdata('id_user');
		$data['id_user'] = $id_user;
		$this->db->insert('tbl_berita', $data);
	}

	public function update($data)
	{
		
		$this->db->update('tbl_berita', $data_edit);

	}

	public function delete($id, $file)
	{
		$this->db->where('id_berita', $id);

		unlink("uploads/".$file);

		$this->db->delete('tbl_berita', array('id_berita' => $id));

	}

	public function berita_user()
	{
		$this->db->select('tbl_berita.*, tbl_kategori_berita.*, tbl_user.id_pegawai, tbl_pegawai.*')
					->from('tbl_berita')
					->join('tbl_kategori_berita', 'tbl_berita.id_kategori_berita = tbl_kategori_berita.id_kategori_berita')
					->join('tbl_user', 'tbl_berita.id_user = tbl_user.id_user')
					->join('tbl_pegawai', 'tbl_user.id_pegawai = tbl_pegawai.id_pegawai')
					->order_by('tbl_berita.id_berita','DESC');
		$list = $this->db->get();

		return $list->result();
	}

	public function get_by_id($id)
	{
		$this->db->select('tbl_berita.*, tbl_kategori_berita.*, tbl_user.id_pegawai, tbl_pegawai.*')
					->from('tbl_berita')
					->join('tbl_kategori_berita', 'tbl_berita.id_kategori_berita = tbl_kategori_berita.id_kategori_berita')
					->join('tbl_user', 'tbl_berita.id_user = tbl_user.id_user')
					->join('tbl_pegawai', 'tbl_user.id_pegawai = tbl_pegawai.id_pegawai')
					->where('id_berita', $id)
					->order_by('tbl_berita.id_berita','DESC');
		$list = $this->db->get();

		return $list->result();
	}

	public function get_edit($id)
	{
		$this->db->select('tbl_berita.*, tbl_kategori_berita.*, tbl_user.id_pegawai, tbl_pegawai.*')
					->from('tbl_berita')
					->join('tbl_kategori_berita', 'tbl_berita.id_kategori_berita = tbl_kategori_berita.id_kategori_berita')
					->join('tbl_user', 'tbl_berita.id_user = tbl_user.id_user')
					->join('tbl_pegawai', 'tbl_user.id_pegawai = tbl_pegawai.id_pegawai')
					->where('id_berita', $id)
					->order_by('tbl_berita.id_berita','DESC');
		$list = $this->db->get();

		return $list->row();
	}

	public function get_pegawai($id_jabatan)
	{	
		$this->db->select('tbl_pegawai.*, tbl_jabatan.*');
		$this->db->where('tbl_pegawai.id_jabatan', $id_jabatan);
		$this->db->from('tbl_pegawai');
		$this->db->join('tbl_jabatan', 'tbl_pegawai.id_jabatan = tbl_jabatan.id_jabatan');
		$list = $this->db->get();

		return $list->result();
	}

	public function get_pelatihan()
	{	
		$this->db->select('tbl_pelatihan.nama_pelatihan, tbl_pelatihan.id_pelatihan');
		$this->db->from('tbl_pelatihan');
		$list = $this->db->get();

		return $list->result();
	}

	public function create_pelatihan($data)
	{
		$this->db->insert_batch('tbl_peserta_pelatihan', $data);
	}

}

/* End of file Model_berita.php */
/* Location: ./application/modules/berita/models/Model_berita.php */