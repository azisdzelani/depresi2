<div class="content-wrapper">

  <section class="content">

    <div class="row">
      <div class="col-sm-12">
        <div class="box box-default">
          <div class="box-body">
            <div class="row">
              <div class="col-sm-12">

                <form class="form-horizontal" method="POST" action="<?=base_url('pelatihan/create_pelatihan') ?>">
                  <div class="box-body">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Pelatihan</label>

                      <div class="col-sm-8">
                        <select name="id_pelatihan" class="form-control">
                          <option value="">--Pilih Pelatihan--</option>
                            <?php foreach($pelatihan as $p): ?>
                          <option value="<?=$p->id_pelatihan ?>"><?=$p->nama_pelatihan ?></option>
                            <?php endforeach; ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>

                      <div class="col-sm-8">
                        <textarea rows="5" name="keterangan" class="form-control" required="" ></textarea>
                      </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover" id="example1">
                        <thead>
                            <tr>
                                <th colspan="7"><p>Pilih Peserta Pelatihan</p></th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Nip</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($pegawai as $r):?>
                            <tr>
                                <td><input type="checkbox" name="id_pegawai[]" value="<?=$r->id_pegawai?>"></td>
                                <td><?=$r->nip?></td>
                                <td><?=$r->nama_lengkap?></td>
                                <td><?=$r->nama_jabatan?></td>
                                <td>    
                                   <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target=" #detailPegawai<?=$r->id_pegawai?>" title="Detail Pegawai">
                                    <i class="fa fa-eye"></i>
                                  </button>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            <tr>
                                <td colspan="7" align="center"><button type="submit" class="btn btn-primary">Simpan</button></td>
                            </tr>
                        </tbody>
                    </table> 

                  </div>
                 </form> 
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>

  </section>
                  
</div>


<!-- modal detail pegawai -->
<?php foreach($pegawai as $r):?>
<div class="modal fade" id="detailPegawai<?=$r->id_pegawai?>" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Detail Pegawai</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    
                
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="3">Detail Pegawai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="1">Nip</td>
                                    <td colspan="2"><?=$r->nip?></td>
                                </tr>
                                <tr>
                                    <td colspan="1">Nama</td>
                                    <td colspan="2"><?=$r->nama_lengkap?></td>
                                </tr>
                                <tr>
                                    <td colspan="1">TTL</td>
                                    <td colspan="2">
                                        <?php
                                            $tgl_lahir = $r->tgl_lahir; 
                                            echo $r->tempat_lahir.', '.$tgl_lahir;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">Alamat</td>
                                    <td colspan="2"><?=$r->alamat_rumah?></td>
                                </tr>
                                <tr>
                                    <td colspan="1">Jenis Kelamin</td>
                                    <td colspan="2">
                                        <?php 
                                        if($r->jenis_kelamin == 'Perempuan')
                                        {
                                            echo "Perempuan";
                                        }
                                        else
                                        {
                                            echo "Laki - Laki";
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
            
        </div>
    </div>
</div>
 <?php endforeach;?>
<!-- end modal detail pegawai -->