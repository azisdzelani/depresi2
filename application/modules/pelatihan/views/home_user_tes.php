<div class="content-wrapper">

  <section class="content">

<!--<div class="row">
      <div class="col-sm-12">
        <div class="well">
          <div class="row">
            <div class="span">
              <img src="http://lan.go.id/images/logo-lan.png">
              <img src="http://lan.go.id/images/banner-header.png" class="pull-right">
              <p></p> 
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <div class="row">
      <div class="col-sm-12">
        <div class="box box-default">
          <div class="box-body">
            <div class="row">
              <div class="col-sm-12">
                <img src="http://lan.go.id/images/logo-lan.png" class="page-header">
                <div class="alert alert-info alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  Segera Ganti Password Lama Anda
                </div>
                <!-- kolom berita -->
              </div>
            </div><br>

            <div class="row">
              <div class="col-sm-6">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active">
                    <a href="#about" aria-controller="home" role="tab" data-toggle="tab"><i class="fa fa-quote-right"></i> About</a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div role="tabpanel" id="about" class="tab-pane active"><br>
                    <p align="justify">Portal Knowledge Management System (KMS) yang beralamat di Bagian Humas dan Informasi LAN
                       merupakan salah satu wahana untuk mempermudah & mempercepat proses berbagi
                       pengetahuan, keahlian, pengalaman dan kolaborasi antar pegawai menuju pada pegawai yang
                       sadar pengetahuan (knowledge workers). Manfaat portal KMS adalah sebagai berikut:</p>
                       <ul align="justify">
                        <li>Mempermudah proses berbagi pengetahuan (Knowledge Sharing) dengan mengunggah (Upload) konten pada Knowledge Repository.
                        </li><br>
                        <li>
                          Mempermudah pegawai dalam mendapatkan pengetahuan yang dibutuhkan atau pengetahuan yang menjadi minat/ketertarikannya dengan mencari dan mengunduh(download) dari Knowledge Repository yang dapat diperkaya oleh semua pegawai sehingga dapat menjadi semacam kamus bagi pengetahuan yang ada dalam portal KMS.
                        </li><br>
                        <li>
                          Mempermudah pencarian pegawai untuk diminta sharing pengalaman, diajak berdiskusitentang suatu pengetahuan atau kolaborasi beberapa keahlian.
                        </li><br>
                        <li>
                          Mempermudah penyampaian pendapat dan pemikiran melalui Menu Blog
                        </li>
                      </ul>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="menu" style="padding-top:30px;">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="small-box bg-green">
                        <div class="inner">
                          <h3>Knowledge</h3>
                          <p>sebagai basis data yang berisi kumpulan pengetahuan dalam bentuk eksplisit, atau yang telah dituangkan ke dalam bentuk digital</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-book"></i>
                        </div>
                        <a href="<?=base_url('dokumen/dokumen_user') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="small-box bg-aqua">
                        <div class="inner">
                          <h3>Discussion</h3>
                          <p>sebagai wadah pegawai untuk berdiskusi segala macam hal yang terkait dengan pekerjaan atau bidang pengetahuan lainnya</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-comments"></i>
                        </div>
                        <a href="<?=base_url('discussion') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- small box -->
                      <div class="small-box bg-red">
                        <div class="inner">
                          <h3>Blog</h3>
                          <p>merupakan kumpulan tulisan yang dihasilkan oleh semua pegawai yang memiliki pengalaman serta keilmuan yang berbeda</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-pencil"></i>
                        </div>
                        <a href="<?=base_url('blog/dokumen_user') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="small-box bg-yellow">
                        <div class="inner">
                          <h3>News</h3>
                          <p>Menampilkan informasi atau berita terkini seputar kegiatan dilingkungan Lembaga Administrasi Negara</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-newspaper-o"></i>
                        </div>
                        <a href="<?=base_url('berita') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>

  </section>
                  
</div>
