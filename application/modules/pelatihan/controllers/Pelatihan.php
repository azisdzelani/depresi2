<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelatihan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array( 'Model_pelatihan' => 'pelatihan'));
	}

	public function index()

	{

		if ( $this->session->userdata('id_jabatan') == 3 ) 
		{
			$id_jabatan_staff = '4';
		}
		else{
			$id_jabatan_staff = '5';
		}
		/*echo "<pre>";
		var_dump($id_jabatan_staff);die();*/


		$data = array(
				'title'		=> 'Pelatihan',
				'pelatihan' => $this->pelatihan->get_pelatihan(),
				'pegawai' 	=> $this->pelatihan->get_pegawai($id_jabatan_staff));

		// 'echo "<pre>";
		  // var_dump($data['pegawai']);die();'
		
		$this->template->content->view('pelatihan_user', $data);
		$this->template->publish('template', array('title'=>'Home'));	
	}

	public function create_pelatihan()
	{
		$id_pelatihan = $this->input->post('id_pelatihan');
		$keterangan = $this->input->post('keterangan');
		$id_pegawai = $this->input->post('id_pegawai');

		for($i = 0; $i < count($id_pegawai); $i++)
		{
			$data[] = array(
				'id_pelatihan'	=> $id_pelatihan,
				'keterangan'	=> $keterangan,
				'id_pegawai'	=> $id_pegawai[$i]
				);
		}

		$this->pelatihan->create_pelatihan($data);
		redirect('pelatihan');
	}

}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */