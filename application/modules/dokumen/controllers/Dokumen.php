<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_dokumen'  => 'dokumen',
								 'model_kategori' => 'kategori'));
	}

	public function index()
	{
		$data = array(
				'title'		=> 'Daftar Master Dokumen',
				'dokumen'	=> $this->dokumen->admin_view());

		$this->template->content->view('dokumen_admin', $data);
		$this->template->publish('template', array('title'=>'Dokumen'));	
	}

	public function create()
	{
		$data = array('title' 	 => 'Form Tambah Dokumen',
					  'kategori' => $this->kategori->lists());

		$this->template->content->view('input_dokumen', $data);
		$this->template->publish('template', array('title'=>'Input Dokumen'));
	}

	public function do_create()
	{
		$this->rules();

		if ($this->form_validation->run() == TRUE or FALSE) {
			$this->create();
		} else {
			$config['upload_path']          = './uploads/';
	       	$config['allowed_types']        = 'docx|doc|pdf';
	        $config['max_size']             = '2000';
	        $config['max_width']            = '2000';
	        $config['max_height']           = '2000';

        	$this->load->library('upload', $config);

        	if ( ! $this->upload->do_upload()){	

         	//file gagal di upload -> kembali ke form tambah dokumen
         	$data = array('title' 	 => 'Form Tambah Dokumen',
					  'kategori' => $this->kategori->lists());

			$this->template->content->view('input_dokumen', $data);
			$this->template->publish('template', array('title'=>'Input Dokumen'));
         	}
         	else{
         	// file berhasil di upload -> lanjutkan INSERT
         	$dokumen = $this->upload->data();
         	$tanggal = date("Y-m-d H:i:s");
			$data = array(
			'id_kategori_dokumen'	=> $this->input->post('kategori'),
			'judul' 				=> $this->input->post('judul'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'tanggal'				=> $tanggal,
			'file' 					=> $this->input->post('file'),
			'file'					=> $dokumen['file_name']
			);

			$this->dokumen->save($data);
			$this->session->set_flashdata('create','Dokumen berhasil ditambah');

			redirect('dokumen');
         }
		}


	}

	public function delete($id)
	{

		$file = $this->input->post('file');

		$this->dokumen->delete($id, $file);
		$this->session->set_flashdata('delete','Dokumen berhasil dihapus');

		redirect('dokumen');
	}
//===================================================== modul admin dokumen end ============================================


//===================================================== modul user dokumen start ===========================================
	public function dokumen_user()
	{
		$id = $this->uri->segment(3); //tangkep param yag dilempar dari url
		$data = array(
				'title'		=> 'Dokumen Pengetahuan',
				'kategori'  => $this->kategori->lists(),
				'dokumen'	=> $this->dokumen->user_view());
		// echo "<pre>";
  		//var_dump($data['kategori']);

		$this->template->content->view('dokumen_user', $data);
		$this->template->publish('template', array('title'=>'Knowledge'));
	}

	public function get_all_kategori()
	{
		$id = $this->uri->segment(3); //tangkep param yag dilempar dari url

		$data['list_kategori'] = $this->kategori->get_all_by_id($id);
		$data['get_kategori'] = $this->kategori->get_kategori($id);
		$data['detail_dokumen'] = $this->dokumen->get_by_id($id);

		// echo "<pre>";
  // 		var_dump($data['list_kategori']);die();

  		$this->template->content->view('get_all_kategori', $data);
		$this->template->publish('template', array('title'=>'Knowledge'));	
	}

	public function get_by_id()
	{
		$id = $this->uri->segment(3);
		$data['detail_dokumen'] = $this->dokumen->get_by_id($id);
		// echo "<pre>";
  		//var_dump($data['detail_dokumen']);die();

  		$this->template->content->view('detail_dokumen', $data);
		$this->template->publish('template', array('title'=>'Knowledge'));	
	}

	public function download($str)
	{
		force_download('./uploads/'.$str, NULL);
	}

	// public function xmlDokumen($filter)
	// {
	// 	$this->db->select('tbl_dokumen.*, tbl_kategori_dokumen.*, tbl_user.id_pegawai, tbl_pegawai.*')
	// 				->from('tbl_dokumen')
	// 				->join('tbl_kategori_dokumen', 'tbl_dokumen.id_kategori_dokumen = tbl_kategori_dokumen.id_kategori_dokumen')
	// 				->where('nama_kategori',$filter)
	// 				->join('tbl_user', 'tbl_dokumen.id_user = tbl_user.id_user')
	// 				->join('tbl_pegawai', 'tbl_user.id_pegawai = tbl_pegawai.id_pegawai');
	// 	$list = $this->db->get();

	// 	$xml = $list->result();

	// 	$data['xml'] = $xml;
	// 	$this->load->view('xml',$data);
	// }

	public function rules()
	{
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterngan', 'required');
		$this->form_validation->set_rules('userfile', 'File', 'required');
		$this->form_validation->set_message('required', '{field} masih kosong silahkan diisi');
		$this->form_validation->set_error_delimiters('<p style="color:red;">*','</p>');

	}

	public function create_user()
	{
		$data = array('title' 	 => 'Share Dokumen',
					  'kategori' => $this->kategori->kategori());
		// echo "<pre>";
  // 		var_dump($data['kategori_user']);die();

		$this->template->content->view('input_dokumen_user', $data);
		$this->template->publish('template', array('title'=>'Input Dokumen'));
	}

	public function do_create_user()
	{
		$this->rules();

		if ($this->form_validation->run() == TRUE or FALSE) {
			$this->create();
		} else {
			$config['upload_path']          = './uploads/';
	       	$config['allowed_types']        = 'docx|doc|pdf';
	        $config['max_size']             = '2000';
	        $config['max_width']            = '2000';
	        $config['max_height']           = '2000';

        	$this->load->library('upload', $config);

        	if ( ! $this->upload->do_upload()){	

         	//file gagal di upload -> kembali ke form tambah dokumen
         	$data = array('title' 	 => 'Form Tambah Dokumen',
					  'kategori' => $this->kategori->lists());

			$this->template->content->view('input_dokumen_user', $data);
			$this->template->publish('template', array('title'=>'Input Dokumen'));
         	}
         	else{
         	// file berhasil di upload -> lanjutkan INSERT
         	$dokumen = $this->upload->data();
         	$tanggal = date("Y-m-d H:i:s");
			$data = array(
			'id_kategori_dokumen'	=> $this->input->post('kategori'),
			'judul' 				=> $this->input->post('judul'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'tanggal'				=> $tanggal,
			'file' 					=> $this->input->post('file'),
			'file'					=> $dokumen['file_name']
			);

			$this->dokumen->save($data);
			$this->session->set_flashdata('create','Dokumen berhasil ditambah');
			redirect('dokumen/dokumen_user');
         }

		}
	}

	public function cari()
	{
		$data['title']="Pencarian";
    	$cari=$this->input->post('cari');
    	$cek =$this->dokumen->cari($cari);
    	if($cek->num_rows()>0){
	        $data['message']="";
	        $data['cari']=$cek->result();
	        $this->template->content->view('cari_dokumen', $data);
			$this->template->publish('template', array('title'=>'Cari Dokumen'));
    	}else{
	        $data['message']="<div class='alert alert-danger'>Data tidak ditemukan</div>";
	        $data['cari']=$cek->result();
	        $this->template->content->view('cari_dokumen', $data);
			$this->template->publish('template', array('title'=>'Cari Dokumen'));
    	}
	}

}

/* End of file Dokumen.php */
/* Location: ./application/modules/dokumen/controllers/Dokumen.php */