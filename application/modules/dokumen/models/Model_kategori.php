<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kategori extends CI_Model {

	public function lists()
	{

		$this->db->select('tbl_kategori_dokumen.*, COUNT(*) as jumlah')
         	     ->from('tbl_dokumen')
                 ->order_by('id_kategori_dokumen', 'desc')
                 ->join('tbl_kategori_dokumen', 'tbl_kategori_dokumen.id_kategori_dokumen = tbl_dokumen.id_kategori_dokumen')
         		 ->group_by('id_kategori_dokumen');

		$list = $this->db->get();

		return $list->result();
	}

	public function get_all_by_id($id)
	{
		$this->db->select('tbl_dokumen.*, tbl_kategori_dokumen.*, tbl_user.id_pegawai, tbl_pegawai.*')
					->from('tbl_dokumen')
					->join('tbl_kategori_dokumen', 'tbl_dokumen.id_kategori_dokumen = tbl_kategori_dokumen.id_kategori_dokumen')
					->join('tbl_user', 'tbl_dokumen.id_user = tbl_user.id_user')
					->join('tbl_pegawai', 'tbl_user.id_pegawai = tbl_pegawai.id_pegawai')
					->where('tbl_dokumen.id_kategori_dokumen', $id)
					->order_by('tbl_dokumen.id_dokumen','desc');
		$list = $this->db->get();

		return $list->result();
	}

	public function kategori()
	{	
		$this->db->select('tbl_kategori_dokumen.*')
					->from('tbl_kategori_dokumen');
		$list = $this->db->get();

		return $list->result();
	}


	public function lists_user()
	{	
		$this->db->select('tbl_kategori_dokumen.*')
					->from('tbl_kategori_dokumen')
					->where('id_kategori_dokumen','13');
		$list = $this->db->get();

		return $list->result();
	}

	public function get_kategori($id)
		{
			$this->db->select('tbl_dokumen.*, tbl_kategori_dokumen.*');
			$this->db->from('tbl_dokumen');
			$this->db->join('tbl_kategori_dokumen', 'tbl_dokumen.id_kategori_dokumen = tbl_kategori_dokumen.id_kategori_dokumen');
			$this->db->where('tbl_dokumen.id_kategori_dokumen', $id);

			$list = $this->db->get();
			return $list->row();
		}

}

/* End of file Model_kategori.php */
/* Location: ./application/modules/dokumen/models/Model_kategori.php */