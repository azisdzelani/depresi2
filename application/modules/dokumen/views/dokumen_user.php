<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-header with-border">
            <h1 class="box-title"><i class="fa fa-book"> <?=$title ?></i></h1>
            
          </div>

          <div class="box-body">
            <div class="col-md-12">
              <?php if($this->session->flashdata('create_kriteria')):?>
                  <div class="alert alert-info">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong><?php echo $this->session->flashdata('create_kriteria'); ?></strong>
                  </div>
              <?php endif; ?>
              <div class="row">
              <a href="<?=base_url('dokumen/create_user') ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Upload Dokumen</a>
              <br><hr>
              
              <div class="col-xs-12" style="padding-bottom: 30px;">
              <h3 class="text-blue">Pencarian Dokumen</h3>
                <form role="search" action="<?=base_url('dokumen/cari')?>" method="post">
                  <div class="input-group">
                        <input type="text" name="cari" class="form-control input-lg" placeholder="Masukan Kata Kunci..." required="">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-lg" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                  </div><!-- /input-group -->
                </form>
              </div>
              
              <p>Pilih Salah Satu Kategori Dokumen.</p>
              <?php foreach($kategori as $result): ?>
              <div class="col-md-3">
                <div class="thumbnail">
                  <div class="caption">
                    <i class="fa fa-bookmark"></i>
                    <a href="<?=base_url('dokumen/get_all_kategori/'.$result->id_kategori_dokumen) ?>" style="color: black"><?=$result->nama_kategori ?></a> 
                    <small class="text-green">[<?=$result->jumlah ?>]</small>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
            </div>
            </div>
          </div>
        <!-- /.box-body -->
    </div>
  </div>
</section>

</div>
