<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          
          <div class="box-body">
            <div class="container">
              <div class="row">

                <div class="col-md-11">
                  <h2 class="text-red page-header"> Kategori : <b><?=$get_kategori->nama_kategori?></b></h2>
                  <div class="row">
                    <?php foreach($list_kategori as $result): ?>
                      <table id="example1" class="table table-hover table-striped">
                        <tbody>
                          <tr>
                            <td width="90%">
                              <h4>
                                <b style="color:blue">[FILE]</b>
                                 - <b><?=$result->file ?></b></h4>
                                <ul class="list-inline">
                                  <li style="padding-right:10px;"><i class="fa fa-user"> Diupload Oleh:</i> <?=$result->nama_lengkap ?> </i></li>
                                  <li><i class="fa fa-calendar"></i> <?=date('d-F-Y', strtotime($result->tanggal)); ?></li>
                                  <li><i class="fa fa-clock-o"></i> <?=date('g:i a', strtotime($result->tanggal)); ?></li>
                                </ul>
                            </td>

                            <td>
                              <a class="btn btn-success" href="<?=base_url()?>dokumen/download/<?=$result->file?>"><i class="fa fa-download fa-fw"></i></a>
  
                              <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal<?=$result->id_dokumen?>"><i class="fa fa-eye"></i>
                              </button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    <?php endforeach; ?> 
                  </div>
                </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>

<!-- Modal -->
<?php foreach($list_kategori as $result): ?>
<div class="modal fade" id="myModal<?=$result->id_dokumen?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Dokumen</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered  table-hover">
          <tr>
            <th colspan="2">
              <h2><?=$result->judul ?></h2>
            </th>
          </tr>
          <tbody>
          <tr class="success">
            <td width="200px"><strong>Di Upload Oleh</strong></td>
            <td><?=$result->nama_lengkap ?></td>
          </tr>
          <tr>
            <td><strong>NIP</strong></td>
            <td><?=$result->nip ?></td>
          </tr>
          <tr class="success">
            <td><strong>Email</strong></td>
            <td><?=$result->email ?></td>
          </tr>
          <tr>
            <td><strong>Kategori</strong></td>
            <td><?=$result->nama_kategori ?></td>
          </tr>
          <tr class="success">
            <td><strong>Deskripsi Singkat</strong></td>
            <td><?=$result->keterangan ?></td>
          </tr>
          <tr>
            <td><strong>Nama File</strong></td>
            <td> <?=$result->file ?></td>
          </tr>
         <!--  <tr>
            <td colspan="2">
              <a class="btn btn-primary pull-right" href="<?=base_url()?>dokumen/download/<?=$result->file?>">
                Download File
              </a>
            </td>
          </tr> -->
                              
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" class="pull-right">Close</button>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>