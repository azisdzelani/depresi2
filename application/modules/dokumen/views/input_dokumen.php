<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"> <?=$title ?></h3>
        </div><br>

        <div class="box-body">
          <div class="row">
            <div class="container">
              <div class="col-sm-12">
                <form class="form-horizontal" method="POST" action="<?=base_url('dokumen/do_create') ?>" enctype="multipart/form-data">
                  <div class="box-body">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Judul</label>

                      <div class="col-sm-6">
                        <input type="text" name="judul" value="<?=set_value('judul'); ?>" class="form-control" id="inputEmail3" placeholder="Masukan Judul Lengkap">
                        <?=form_error('judul') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Kategori</label>

                      <div class="col-sm-6">
                        <select name="kategori" class="form-control">
                          <option value="">--Pilih Kategori--</option>
                          <?php foreach($kategori as $result): ?>
                            <option value="<?=$result->id_kategori_dokumen ?>"><?=$result->nama_kategori  ?></option>
                          <?php endforeach; ?>
                        </select>
                        <?=form_error('kategori') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Deskrpsi Singkat</label>

                      <div class="col-sm-6">
                        <textarea rows="5" name="keterangan" class="form-control">
                          <?=set_value('keterangan'); ?>  
                        </textarea>
                        <?=form_error('keterangan') ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Pilih Dokumen</label>

                      <div class="col-sm-6">
                        <input type="file" name="userfile" value="<?=set_value('userfile'); ?>" class="form-control" id="inputEmail3">
                        <?=form_error('userfile') ?>
                      </div>
                    </div>


                    <div class="form-group">
                      <div class="col-sm-offset-7 col-sm-6">
                        <button type="submit" class="btn btn-primary">Save</button> 
                        <button type="reset" class="btn btn-primary">Cancel</button>
                      </div>
                    </div>

                  </div>
                 </form> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      </div>
    </div>
  </section>

</div>
