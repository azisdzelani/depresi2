<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
       
          <div class="box-body">
            <div class="container">
              <div class="row">
              <div class="col-md-8">          
                <?php foreach($detail_dokumen as $result): ?>
                <div class="row">
                  <table class="table table-bordered  table-hover">
                    <tr>
                      <th colspan="2">
                        <h2><?=$result->judul ?></h2>
                      </th>
                    </tr>
                    <tbody>
                    <tr class="success">
                      <td width="200px"><strong>Di Upload Oleh</strong></td>
                      <td><?=$result->nama_lengkap ?></td>
                    </tr>
                    <tr>
                      <td><strong>NIP</strong></td>
                      <td><?=$result->nip ?></td>
                    </tr>
                    <tr class="success">
                      <td><strong>Email</strong></td>
                      <td><?=$result->email ?></td>
                    </tr>
                    <tr>
                      <td><strong>Kategori</strong></td>
                      <td><?=$result->nama_kategori ?></td>
                    </tr>
                    <tr class="success">
                      <td><strong>Deskripsi Singkat</strong></td>
                      <td><?=$result->keterangan ?></td>
                    </tr>
                    <tr>
                      <td><strong>Nama File</strong></td>
                      <td> <?=$result->file ?> <a class="btn btn-primary" href="<?=base_url()?>dokumen/download/<?=$result->file?>">
                          <i class="fa fa-download"></i> Download File
                        </a></td>
                    </tr>
                   <!--  <tr>
                      <td colspan="2">
                        <a class="btn btn-primary pull-right" href="<?=base_url()?>dokumen/download/<?=$result->file?>">
                          Download File
                        </a>
                      </td>
                    </tr> -->
                                        
                    </tbody>
                  </table>
                  <!-- <div class="col-md-4">
                    <dl>
                      <dt class="text-red"><i class="fa fa-clock-o"></i> Date</dt>
                      <dd><?=$result->tanggal ?></dd><br>
                      <dt class="text-red"><i class="fa fa-user"></i> Uploaded by</dt>
                      <dd><?=$result->nama_lengkap ?></dd><br>
                      <dt class="text-red"><i class="fa fa-fire"></i> Category</dt>
                      <dd><?=$result->nama_kategori ?></dd><br>
                    </dl> 
                  </div> -->

                  <!-- <div class="col-md-8">
                    <dl>
                      <dt class="text-red"><i class="fa fa-info-circle"></i> Description</dt>
                      <dd><?=$result->keterangan ?></dd><br>
                      
                      <dt class="text-red">Download/Open</dt>
                      <dd><a href="<?=base_url()?>dokumen/download/<?=$result->file?>"><i class="fa fa-cloud-download"> <?=$result->file ?></i></a></dd><br>

                    </dl>
                  </div> -->
                </div>
                <?php endforeach; ?> 
              </div>

              <div class="col-md-3">
                <!-- <div class="robot-fly">
                  <div class="panel panel-warning">
                    <div class="panel-body">
                      <div class="shared-dokumen">
                        <p>Punya Dokumen Pengetahuan?</p>
                        <img class="btn btn-block" src="<?=base_url() ?>assets/images/book.png">
                          <a href="<?=base_url('dokumen/create_user') ?>" class="btn btn-block btn-info btn-lg">Share Disini..</a>
                      </div>
                    </div>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
