<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab"><i class="fa fa-book"> <?=$title ?></i></a></li>
            </ul>
            <div class="tab-content"><br>
              <!-- Font Awesome Icons -->
              <div class="tab-pane active" id="user">
              <?php if($this->session->flashdata('create')):?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo $this->session->flashdata('create'); ?></strong>
                </div>
              <?php elseif($this->session->flashdata('delete')):?>
                  <div class="alert alert-info">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                  </div>
              <?php endif; ?>

                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Uploader</th>
                  <th>Kategori Dokumen</th>
                  <th>Deskripsi Singkat</th>
                  <th>Tanggal Upload</th>
                  <th>File Digital</th>
                  <th><a class="btn btn-large btn-block btn-primary" href="<?=base_url('dokumen/create') ?>"><i class="fa fa-plus-square"> Tambah Dokumen</i></a></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $i=0;
                    foreach ($dokumen as $result) { ?>
                <tr>
                  <td><?=$result->nama_lengkap?></td>
                  <td><?=$result->nama_kategori?></td>
                  <td><?=$result->keterangan?></td>
                  <td><?=$result->tanggal?></span></td>
                  <td><?=$result->file?></span></td>
                  <td>
                    <a href="<?=base_url('dokumen/delete/'.$result->id_dokumen)?>" class="btn btn-small btn-block btn-danger"><i class="btn-icon-only icon-remove">
                      Hapus</i></a>
                  </td>
                </tr>
                <?php } ?>   
                </tbody>
              </table>
            </div>
              <!-- /#fa-icons -->

              <!-- glyphicons-->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->  
    </div>
    </div>
  </section>
</div>