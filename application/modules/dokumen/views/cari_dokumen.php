<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          
          <div class="box-body">
            <div class="container">
              <div class="row">

                <div class="col-md-8">
                  <h1 class="text-red"><b>Search Result</b></h1><hr>
                  <p><?php echo $message;?></p>
                  <div class="form-group has-success">
                    <form action="<?=base_url('dokumen/cari') ?>" method="post">
                      <div class="input-group">
                        <input type="text" name="cari" class="form-control" required="" placeholder="masukan kata kunci...">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-search"></i></button>
                          </span>
                      </div>
                    </form>
                  </div>

                  <div class="row">
                    <h4>Pecarian dengan kata kunci:&nbsp; <i class="text-danger"><strong><?=$this->input->post('cari');?></strong></i></h4> 
                    <?php foreach($cari as $result): ?>
                        <h3 style="padding-left:20px">
                          <a href="<?=base_url('dokumen/get_by_id/'.$result->id_dokumen)?>"> <?=$result->file ?></a>
                        </h3>
                        <small>
                          <ul class="list-inline" style="padding-left:20px">
                            <li style="padding-right:10px;"><i class="fa fa-user"></i> <?=$result->nama_lengkap ?></li>
                            <li><i class="fa fa-calendar"></i> <?=date('d-m-Y', strtotime($result->tanggal)); ?></li>
                            <li><i class="fa fa-tag"></i>  Category : <?=$result->nama_kategori ?></li>
                          </ul>
                        </small>
                          <?php
                            $dokumen = $result->keterangan;
                            $ket_dokumen = character_limiter($dokumen, 200);
                          ?>
                        <p style="padding-left:20px"><?=$ket_dokumen ?></p>
                      <?php endforeach; ?> 
                  </div>
                </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
