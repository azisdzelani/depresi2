<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_berita' => 'berita',
								 'model_kategori' => 'kategori'));
	}

	// user view berita
	public function index()
	{
		$data = array('title'  => 'Berita Terbaru',
					  'berita_user' => $this->berita->berita_user());
		// echo "<pre>";
  		//var_dump($data['berita_user']);

		$this->template->content->view('berita_user', $data);
		$this->template->publish('template', array('title'=>'Berita'));	
	}

	public function get_by_id()
	{
		$id = $this->uri->segment(3);
		$data['detail_berita'] = $this->berita->get_by_id($id);
		// echo "<pre>";
  // 		var_dump($data['detail_berita']);die();

  		$this->template->content->view('detail_berita', $data);
		$this->template->publish('template', array('title'=>'Berita'));	
	}

//======================================= berita user end =====================================================
	

//======================================= admin modul berita ==================================================
	public function admin_view()
	{
		$data['title'] = 'Data Master Berita';
		$data['berita'] = $this->berita->lists();
		 // echo "<pre>";
   //      var_dump($data['berita']);
  
		$this->template->content->view('berita_admin', $data);
		$this->template->publish('template', array('title'=>'Berita'));	
	}

	public function create()
	{
		$data = array('title' => 'Form Tambah Berita',
					 'kategori' => $this->kategori->lists());
  
		$this->template->content->view('input_berita', $data);
		$this->template->publish('template', array('title'=>'Berita'));	
	}

	public function do_create()
	{

		$this->rules();
		if ($this->form_validation->run() == FALSE) {

			$data = array('title' => 'form input berita',
					 	  'kategori' => $this->kategori->lists());
  
			$this->template->content->view('input_berita', $data);
			$this->template->publish('template', array('title'=>'Berita'));	
			
		} else {
			
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'png|jpg|jpeg';
			$config['max_size']             = '300';
			$config['max_width']            = '2000';
			$config['max_height']           = '2000';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload()){	

	         	//file gagal di upload -> kembali ke form tambah dokumen
				$data = array('title' => 'Data Master Berita',
					'kategori' => $this->kategori->lists());

				$this->template->content->view('input_berita', $data);
				$this->template->publish('template', array('title'=>'Berita'));	
			}
			else{
	         	// file berhasil di upload -> lanjutkan INSERT
				$dokumen = $this->upload->data();
				$tanggal = date("Y-m-d H:i:s");

				$data = array(
					'id_kategori_berita'	=> $this->input->post('kategori'),
					'judul' 				=> $this->input->post('judul'),
					'isi'					=> $this->input->post('isi'),
					'tanggal'				=> $tanggal,
					'gambar' 				=> $this->input->post('file'),
					'gambar'					=> $dokumen['file_name']
					);

				$this->berita->save($data);
				$this->session->set_flashdata('create','Berita berhasil ditambah');

				redirect('berita/admin_view');
			}

		}

		
	}

	public function edit($id)
	{
		$data['title'] = 'Edit Berita';

		$data['berita'] = $this->berita->get_edit($id);
		$data['kategori'] = $this->kategori->lists();

		// echo "<pre>";
  		//var_dump($data['kategori']);

		$this->template->content->view('edit_berita', $data);
		$this->template->publish('template', array('title'=>'Edit Berita'));
	}

	public function update()
	{	
		$this->rules();
		if ($this->form_validation->run() == FALSE) {

			$data = array('title' => 'form input berita',
					 	  'kategori' => $this->kategori->lists());
  
			$this->template->content->view('edit_berita', $data);
			$this->template->publish('template', array('title'=>'Berita'));	
			
		} else {

			if($_FILES['userfile']['name'] != '' ){

				//form submit dengan gambar diisi
				// load uploading file library
				$config['upload_path']          = './uploads/';
				$config['allowed_types']        = 'png|jpg|jpeg';
				$config['max_size']             = '300';
				$config['max_width']            = '2000';
				$config['max_height']           = '2000';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload()){	

		         	//file gagal di upload -> kembali ke form tambah dokumen
					$data = array('title' => 'form input berita',
						 	  'kategori' => $this->kategori->lists());
	  
					$this->template->content->view('edit_berita', $data);
					$this->template->publish('template', array('title'=>'Berita'));	
				}
				else{
		         	// file berhasil di upload -> lanjutkan INSERT
					$gambar = $this->upload->data();
					$tanggal = date("Y-m-d H:i:s");

					$data = array(
						'id_berita'				=> $this->input->post('id_berita'),
						'id_kategori_berita'	=> $this->input->post('kategori'),
						'judul' 				=> $this->input->post('judul'),
						'isi'					=> $this->input->post('isi'),
						'tanggal'				=> $tanggal,
						'gambar' 				=> $this->input->post('file'),
						'gambar'				=> $gambar['file_name']
						);

					$this->berita->update($data);
					$this->session->set_flashdata('update','Berita berhasil diupdate');

					redirect('berita/admin_view');
				}

			}else{

				$tanggal = date("Y-m-d H:i:s");
				$data = array(
					'id_berita'				=> $this->input->post('id_berita'),
					'id_kategori_berita'	=> $this->input->post('kategori'),
					'judul' 				=> $this->input->post('judul'),
					'isi'					=> $this->input->post('isi'),
					'tanggal'				=> $tanggal);

				$this->berita->update($data);
				$this->session->set_flashdata('update','Berita berhasil diupdate');

				redirect('berita/admin_view');
			}	

		}

	}

	public function delete($id)
	{
		$file = $this->input->post('file');

		$this->berita->delete($id, $file);
		$this->session->set_flashdata('delete','Berita berhasil dihapus');

		redirect('berita/admin_view');
	}

	public function rules()
	{
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('isi', 'Isi Berita', 'required');
		// $this->form_validation->set_rules('userfile', 'Berita Image', 'required');
		$this->form_validation->set_message('required', '{field} masih kosong silahkan diisi');
		$this->form_validation->set_error_delimiters('<p style="color:red;">*','</p>');

	}

}

/* End of file Berita.php */
/* Location: ./application/modules/berita/controllers/Berita.php */