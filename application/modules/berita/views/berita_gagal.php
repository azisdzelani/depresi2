<div class="content-wrapper">
 
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-header with-border">
            <h1 class="box-title"><i class="fa fa-newspaper-o fa-2x"> <?=$title?></i></h1>
          </div>

          <div class="box-body">
            <div class="container">
              <div class="row">

                <div class="col-md-8">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <?php foreach($berita_user as $result): ?>
                      <a href="<?=base_url('berita/get_by_id/'.$result->id_berita)?>"><h3><?=$result->judul ?></h3></a>
                      <div class="info-meta">
                        <ul class="list-inline">
                          <li><i class="fa fa-clock-o"></i><?=$result->tanggal ?> </li>
                          <li><i class="fa fa-user"></i> Posting by <?=$result->nama_lengkap ?></li>
                          <li class="pull-right">Category : <?=$result->nama_kategori ?></li>
                        </ul>
                      </div>
                      <div class = "media">
                       <a class = "pull-left" href = "#">
                        <?php 
                            $berita_image = ['src'     => 'uploads/'. $result->gambar,
                                             'width'   => '50',
                                             'height' => '50'];
                            echo img($berita_image);
                         ?>
                      </a>
                      <div class = "media-body">
                        <?php
                            $berita = $result->isi;
                            $berita = character_limiter($berita, 200);
                         ?>
                        <p style="text-align:justify"><?=$berita; ?>
                        </a></p>
                      </div>
                    </div><hr>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>

             <!--  <div class="col-md-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="text-center">Latest News</h4>
                </div>
                <div class="panel-body">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
              </div>
              </div> -->
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
