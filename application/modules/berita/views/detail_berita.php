<div class="content-wrapper">
  
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-body">
            <div class="container">
              <div class="row">

                <div class="col-md-11">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <?php foreach($detail_berita as $result): ?>
                      <h3><?=$result->judul ?></h3>
                      <div class="info-meta">
                        <ul class="list-inline">
                          <li><i class="fa fa-user"></i> Oleh: <b><?=$result->level_user ?></b></li>
                          <li><i class="fa fa-calendar-o"></i> Diposkan pada: <b><?=date('d F Y', strtotime($result->tanggal)); ?></b></li>
                          <li class="pull-right"><b>Kategori:</b> <em><?=$result->nama_kategori ?></em></li>
                        </ul>
                      </div>
                      <hr>
                        <p style="text-align:justify">
                          <img src="<?=base_url('uploads/'.$result->gambar)?>" width="300px" alt="..." style="float:left;padding:5px 20px 5px 10px;"> <?=$result->isi ?>
                        </p>
                      <hr>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>

            <!--   <div class="col-md-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="text-center">Latest News</h4>
                </div>
                <div class="panel-body">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
              </div>
              </div> -->
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
