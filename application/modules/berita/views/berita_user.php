<div class="content-wrapper">
 
  <section class="content">
    <div class="row">
      <div class="col-sm-12"> 
        <div class="box box-default color-palette-box">
          <div class="box-header with-border">
            <h1 class="box-title"><i class="fa fa-newspaper-o"> <?=$title?></i></h1>
          </div>

          <div class="box-body">
            <div class="container">
              <div class="row">

                <div class="col-md-8">
                  <?php foreach($berita_user as $result): ?>
                  <ul class="timeline">
                    <!-- timeline time label -->
                    <li class="time-label">
                      <span class="bg-red">
                        <?=date('d F Y', strtotime($result->tanggal)); ?>
                      </span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>

                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> <?=date('H:i a', strtotime($result->tanggal)); ?></span>

                        <h3 class="timeline-header"> <b><?=strtoupper($result->judul )?></b></h3>

                        <div class="timeline-body">
                          <?php
                            $berita = $result->isi;
                            $berita = character_limiter($berita, 400);
                         ?>
                          <?=$berita; ?>
                        </div>
                        <div class="timeline-footer">
                          <a class="btn btn-primary btn-xs" href="<?=base_url('berita/get_by_id/'.$result->id_berita)?>">Read more</a>
                          <a class="btn btn-danger btn-xs"><i class="fa fa-tag"> <em><?=$result->nama_kategori ?></em></i></a>
                        </div>
                      </div>
                    </li>
                    <!-- END timeline item -->
                  </ul>
                  <?php endforeach; ?>
                  <!-- END timeline item -->
                </div>

             <!--  <div class="col-md-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="text-center">Latest News</h4>
                </div>
                <div class="panel-body">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
              </div>
              </div> -->
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

</div>
