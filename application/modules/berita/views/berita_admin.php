<div class="content-wrapper">
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab"><i class="fa fa-newspaper-o"> <?=$title ?></i></a></li>
            </ul>
            <div class="tab-content"><br>
              <!-- Font Awesome Icons -->
              <div class="tab-pane active" id="user">
              <?php if($this->session->flashdata('create')):?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo $this->session->flashdata('create'); ?></strong>
                </div>
              <?php elseif($this->session->flashdata('update')):?>
                  <div class="alert alert-info">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong><?php echo $this->session->flashdata('update'); ?></strong>
                  </div>
              <?php elseif($this->session->flashdata('delete')):?>
                  <div class="alert alert-info">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong><?php echo $this->session->flashdata('delete'); ?></strong>
                  </div>
              <?php endif; ?>

                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul Berita</th>
                  <th>Kategori</th>
                  <th>Gambar</th>
                  <th>Date</th>
                  <th><a class="btn btn-large btn-primary" href="<?=base_url('berita/create') ?>"><i class="fa fa-plus-square"> Tambah Berita</i></a></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $i=0;
                    foreach ($berita as $result) { ?>
                <tr>
                  <td width="2%"><?=++$i?></td>
                  <td width="28%"><?=$result->judul?></td>
                  <td><?=$result->nama_kategori?></td>
                  <td>
                    <?php
                      $berita_image = ['src' => 'uploads/'. $result->gambar,
                                       'width' => '100',
                                       'class' => 'img-rounded',
                                       'height' => '100'];
                      echo img($berita_image)
                    ?>
                  </td>
                  <td><?=$result->tanggal?></td>
                  <td width="10%">
                    <a href="<?=base_url('berita/edit/'.$result->id_berita)?>" class="btn btn-small btn-info"><i class="btn-icon-only icon-pencil">
                      Edit</i></a>

                    <a href="<?=base_url('berita/delete/'.$result->id_berita)?>" class="btn btn-small btn-danger"><i class="btn-icon-only icon-remove">
                      Hapus</i></a>
                  </td>
                </tr>
                          <?php } ?>   
                </tbody>
              </table>
              </div>
              <!-- /#fa-icons -->
            </div>
            <!-- /.tab-content -->
        </div>
          <!-- /.nav-tabs-custom -->  
      </div>
    </div>
  </section>
</div>